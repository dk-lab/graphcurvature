Graph Curvature Toolkit
=======================

## Summary
This repository contains the Graph Curvature Toolkit. This package generates graphs and calculates the Ollivier-Ricci curvature as described in the paper ["Ollivier-Ricci Curvature Convergence in Random Geometric Graphs",
*Physical Review Research, v.3, 013211, 2021.*](http://dx.doi.org/10.1103/PhysRevResearch.3.013211) Developers may wish to either add their own graph generation algorithm or export certain subroutines, such as the one used to calculate Wasserstein distance on a graph. These methods were tested on Gentoo 2.7 and CentOS 7.7 and should be compatible with any Linux distribution. If you encounter errors during installation, check the versions of the required packages listed below.

## Environment Variables
There are several environment variables one should export in the $HOME/.bashrc file before installation:

1\. **CURVATURE\_HOME** - This will be the directory where the package is compiled. Some options are

        $ export CURVATURE_HOME=$HOME/graphcurvature
        $ export CURVATURE_HOME=/opt/graphcurvature
        $ export CURVATURE_HOME=$PWD

2\. **FBALIGN** - This is the alignment of the FastBitset used to represent graphs. If your CPU supports AVX2 (AVX-512) it will be set automatically to 256 (512). If not set, the installation script will determine this for you. Options are

        $ export FBALIGN=64
        $ export FBALIGN=256
        $ export FBALIGN=512

3\. **PLATFORM** - This is the name of the system on which you are installing the package. If you are using a workstation, try

        $ export PLATFORM=$HOSTNAME

   whereas if you are on a cluster, you should set it to the name of the partition/queue you are using, e.g.

        $ export PLATFORM=general

   supposing that "general" is the name of the default partition.

4\. **BOOST\_ROOT** - This is the installation directory of Boost. See below for more information on dependencies. If you installed Boost using a package manager, or you installed it from source without specifying the installation directory, you do not need to set this variable.

5\. **MSKHOME** and **MSKVER** - These are the installation directory and version number of MOSEK. The installation directory should contain the subdirectory mosek/$MSKVER when installed correctly.

6\. **CUDA\_HOME** and **CUDA\_ARCH** - These are required if you install with the flag '--enable-cuda' (recommended). The former is the installation directory of CUDA, while the latter specifies the compute capability (found on wiki:CUDA).  E.g. for a Tesla V100, the compute capability is 7.0, so use sm_70 as the flag variable.

## Prerequisites
The following packages should be installed prior to installation of this package. Other versions may be supported but they have not been tested.

1\. **Boost** v1.72.0

   Ubuntu/Debian:

        $ sudo apt-get install libboost-all-dev

   Fedora/RedHat/CentOS:

        $ sudo yum install boost boost-devel

   Arch:

        $ sudo pacman -S boost boost-libs

   Gentoo:

        $ sudo emerge boost

   If you wish to install a particular version from source, visit the [Boost](www.boost.org) webpage and follow their instructions.

2\. **HDF5** v1.12.0

   Ubuntu/Debian:

        $ sudo apt-get install libhdf5-dev

   Fedora/RedHat/CentOS:
   
        $ sudo yum install hdf5-devel

   Arch:

        $ sudo pacman -S hdf5

   Gentoo:

        $ sudo emerge hdf5

3\. **GCC/GCC++** v7.5.0

4\. **CUDA** v10.2.89

   CUDA may be installed from the NVIDIA website or through a package manager. Consult the documentation for proper installation procedures.

5\. **MOSEK** v9.0.103

   MOSEK may be downloaded from the website and it requires a license to use.  A free academic license may be obtained after registration. Consult the documentation for proper usage.

6\. **Autotools** v2.69

   Ubuntu/Debian:

        $ sudo apt-get install autoconf automake

   Fedora/RedHat/CentOS:

        $ sudo yum install autoconf automake

   Arch:

        $ sudo pacman -S autoconf automake

   Gentoo:

        $ sudo emerge autoconf automake

   To determine the version currently installed, run

        $ autoconf --version

7\. **FastMath** v3.1

   Install the FastMath package by consulting the documentation at <bitbucket.org/dk-lab/2015_code_fastmath>.

## Compilation and Installation
The script **install** will compile the code. Run it with one of the following:

        $ ./install
        $ ./install --enable-cuda

Once it has been executed, if there are no errors you may run

        $ ./test

to run an example.  Make sure all environment variables are set and all prerequisites are satisfied before attempting this.  To clean up the directory, you can reverse the above with

        $ make distclean

## Maintainers
Please direct all questions and report all bugs to Will Cunningham at < wjcunningham7 AT gmail DOT com >

&copy; Will Cunningham 2018-2020
