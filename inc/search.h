#ifndef SEARCH_H_
#define SEARCH_H_

#include "distances.h"
#ifdef CUDA_ENABLED
#include <thrust/device_ptr.h>
#include <thrust/sort.h>
#endif
#include "resources.h"

/////////////////////////////
//(C) Will Cunningham 2019 //
//    Perimeter Institute  //
/////////////////////////////

#ifdef CUDA_ENABLED
struct GA_DATA {
	CUdeviceptr z;
	CUdeviceptr k;
	CUdeviceptr links;
	CUdeviceptr linkidx;
	CUdeviceptr measures;
	CUdeviceptr generators;

	CUdeviceptr nodes, nodesize;
	CUdeviceptr openlist;
	CUdeviceptr hash;
	CUdeviceptr heapsize, heapindex;
	CUdeviceptr sortlist, sortlistsize;
	CUdeviceptr sortlist2, sortlistsize2;
	CUdeviceptr heapinsertlist, heapinsertsize;
	CUdeviceptr optimalnodes, optimalsize;
	CUdeviceptr mindist;
};

template <int dummy>
__global__ void empty() {}

class curvature_context_t 
{
	protected:
	CuResources cu;
	cudaDeviceProp _props;
	int _ptx_version;
	cudaEvent_t timer[2];
	cudaEvent_t _event;

	public:
	curvature_context_t(const CuResources &_cu)
	{
		cu.gpus_active = _cu.gpus_active;
		for (unsigned i = 0; i < cu.gpus_active; i++) {
			cu.cuDevice[i] = _cu.cuDevice[i];
			cu.cuContext[i] = _cu.cuContext[i];
		}

		cuCtxPushCurrent(cu.cuContext[0]);
		cudaGetDeviceProperties(&_props, 0);

		cudaFuncAttributes attr;
		cudaFuncGetAttributes(&attr, empty<0>);
		_ptx_version = attr.ptxVersion;

		cudaEventCreate(&timer[0]);
		cudaEventCreate(&timer[1]);
		cudaEventCreate(&_event);
		cuCtxPopCurrent(&cu.cuContext[0]);
	}

	curvature_context_t(const curvature_context_t& other) : curvature_context_t(other.cu) { }

	~curvature_context_t()
	{
		cuCtxPushCurrent(cu.cuContext[0]);
		cudaEventDestroy(timer[0]);
		cudaEventDestroy(timer[1]);
		cudaEventDestroy(_event);
		cuCtxPopCurrent(cu.cuContext);
		for (unsigned i = 0; i < cu.gpus_active; i++) {
			cu.cuDevice[i] = 0;
			cu.cuContext[i] = 0;
		}
	}

	void push_device(unsigned dev)
	{
		assert (dev < MAX_GPUS);
		cuCtxPushCurrent(cu.cuContext[dev]);
	}

	void pop_device(unsigned dev)
	{
		assert (dev < MAX_GPUS);
		cuCtxPopCurrent(&cu.cuContext[dev]);
	}

	void sync_all_gpus()
	{
		for (unsigned i = 0; i < cu.gpus_active; i++) {
			if (cu.gpus_active > 1)
				push_device(i);
			checkCudaErrors(cuCtxSynchronize());
			if (cu.gpus_active > 1)
				pop_device(i);
		}
	}

	unsigned gpus_active() { return cu.gpus_active; }

	virtual const cudaDeviceProp& props() const { return _props; }

	virtual int ptx_version() const { return _ptx_version; }

	virtual cudaStream_t stream() { return 0; }

	virtual void synchronize() { checkCudaErrors(cuCtxSynchronize()); }

	virtual cudaEvent_t event() { return _event; }

	virtual void timer_begin() { cudaEventRecord(timer[0], 0); }

	virtual double timer_end()
	{
		cudaEventRecord(timer[1], 0);
		cudaEventSynchronize(timer[1]);
		float ms;
		cudaEventElapsedTime(&ms, timer[0], timer[1]);
		return ms / 1.0e3;
	}
};
#endif

void dfsearch_v2(const Nodes &nodes, const Bitvector &adj, const unsigned N, const unsigned index, const int id, unsigned &elements);

int shortestPathUnweighted(Bitvector &adj, const unsigned * const k, const unsigned N, FastBitset &awork, std::vector<unsigned> &next, float * const dwork, const unsigned source, const unsigned sink);

void shortestPathWeighted(Bitvector &adj, const Nodes &nodes, const Region region, const unsigned N, const float * const measures, float * const dwork, bool * const spwork, const unsigned source, const bool gpu);

float shortestPathWeighted_v2(Bitvector &adj, const Nodes &nodes, const Region region, const unsigned N, const float * const measures, PriorityQueue<unsigned, float> &frontier, float * const dwork, FastBitset &neighbors, const unsigned source, const unsigned sink, const bool gpu);

void shortestPathWeighted_v3(Bitvector &adj, const Nodes &nodes, const Region region, const unsigned N, const float * const measures, FastPQ<float> &frontier, float * const dwork, FastBitset &neighbors, const unsigned source, std::vector<unsigned> sinks);

#ifdef CUDA_ENABLED

template<Region region, bool weighted>
void GA_step(GA_DATA &d, std::vector<std::priority_queue<heap_t, std::vector<heap_t>, std::greater<heap_t>>> &pq, double * const distances, node_t * const h_nodes, std::vector<unsigned> &h_hash, std::vector<heap_t> &h_optimalnodes, const unsigned N, const unsigned source, std::vector<std::pair<unsigned,unsigned>> &psinks, unsigned &sinkidx, curvature_context_t &context, float &shortest, const float epsilon, const unsigned k_max, unsigned &round, bool &solved, Stopwatch * const watches);

bool shortestPathWeighted_v5(pinned_vector<unsigned> &h_links, pinned_vector<uint64_t> h_linkidx, const Nodes &nodes, double * const distances, float * const measures, pinned_vector<float> &h_measures, const unsigned N, const Region region, const float epsilon, const unsigned k_max, std::vector<unsigned> &sources, std::vector<unsigned> &sinks, const bool weighted, Resources * res, CuResources &cu);

#endif

#endif
