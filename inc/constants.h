#ifndef CONSTANTS_H_
#define CONSTANTS_H_

/////////////////////////////
//(C) Will Cunningham 2018 //
//         DK Lab          //
// Northeastern University //
/////////////////////////////

#define RNG_THERMAL 1000

#define INF (1e20)	//Infinity

#define IS_INF(x) (x > 1e18)	//There are numerical issues when comparing x==INF

#define TOL (1e-10)	//Zero

#ifdef CUDA_ENABLED

#include <cuComplex.h>

#define GPU_BLOCK_SIZE 128	//DO NOT EDIT
#define LINKS_PER_THREAD 4	//Should be a power of 2
#define GPU_MIN 1024		//DO NOT EDIT

#define OPEN_LIST_SIZE 500000	//These two values need to be at least as big as N when running
#define NODE_LIST_SIZE 500000
#define NUM_BLOCK (28)	//Should be a multiple of the number of SMs
#define NUM_THREAD (32 * 4)	//Should be a multiple of 32, probably should not edit
#define NUM_TOTAL (NUM_BLOCK * NUM_THREAD)	//DO NOT EDIT
#define VALUE_PER_THREAD 1	//DO NOT EDIT
#define NUM_VALUE (NUM_TOTAL * VALUE_PER_THREAD)	//DO NOT EDIT
#define HEAP_CAPACITY (OPEN_LIST_SIZE / NUM_TOTAL)	//DO NOT EDIT

#define MAX_SMALL 128	//DO NOT EDIT

#define THREAD_ID (threadIdx.x)	//DO NOT EDIT
#define GLOBAL_ID (THREAD_ID + NT * blockIdx.x)	//DO NOT EDIT
#define BLOCK_ID (blockIdx.x)	//DO NOT EDIT

#else

#define GPU_MIN 0		//DO NOT EDIT

#endif

#include "generators.h"

#endif
