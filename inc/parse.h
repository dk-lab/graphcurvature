#ifndef PARSE_H_
#define PARSE_H_

#include "curvature.h"

/////////////////////////////
//(C) Will Cunningham 2019 //
//    Perimeter Institute  //
/////////////////////////////

Properties parseArgs(int argc, char **argv)
{
	Properties props = Properties();
	int c, longIndex;

	static const char *optString = ":a:b:C:c:g:hN:n:M:m:s:S:t:Tvw";
	static const struct option longOpts[] = {
		{ "alpha",	required_argument,	NULL, 'a' },
		{ "beta",	required_argument,	NULL, 'b' },
		{ "classic",	no_argument,		NULL, 'C' },
		{ "chi",	required_argument,	NULL, 'c' },
		{ "gpu",	no_argument,		NULL, 'g' },
		{ "help",	no_argument,		NULL, 'h' },
		{ "manifold",	required_argument,	NULL, 'm' },
		{ "neighborhood",required_argument,	NULL, 'N' },
		{ "nodes",	required_argument,	NULL, 'n' },
		{ "search-method",required_argument,	NULL, 'M' },
		{ "psi",	required_argument,	NULL, 'p' },
		{ "seed",	required_argument,	NULL, 's' },
		{ "stretch",	no_argument,		NULL, 'S' },
		{ "target-separation",required_argument,NULL, 't' },
		{ "test",	no_argument,		NULL, 'T' },
		{ "weighted",	no_argument,		NULL, 'w' },
		{ "visualize",	no_argument,		NULL, 'v' },
		{ NULL,		0,			0,     0  }
	};

	try {
		while ((c = getopt_long(argc, argv, optString, longOpts, &longIndex)) != -1) {
			switch (c) {
			case 'a':	//Scaling parameter for delta
				props.alpha = atof(optarg);
				if (props.alpha <= 0.0)
					throw CurvatureException("Invalid argument for parameter '--alpha'");
				break;
			case 'b':	//Scaling parameter for epsilon
				props.beta = atof(optarg);
				if (props.beta <= 0.0)
					throw CurvatureException("Invalid argument for parameter '--beta'");
				break;
			case 'C':	//Classic Ollivier curvature, epsilon = delta
				props.classic = true;
				break;
			case 'c':	//Exponent associated with delta
				props.chi = atof(optarg);
				break;
			case 'g':	//Enable GPU algorithms
				props.use_gpu = true;
				#ifndef CUDA_ENABLED
				throw CurvatureException("Cannot use GPU. Reconfigure with --enable-cuda.\n");
				#endif
				break;
			case 'M':	//Search Method
				if (!strcmp("bfs", optarg))
					props.sm = BFS;
				else if (!strcmp("dijkstra", optarg))
					props.sm = DIJKSTRA;
				else if (!strcmp("a_star", optarg))
					props.sm = ASTAR;
				else if (!strcmp("hybrid_astar", optarg))
					props.sm = HYBRID_ASTAR;
				else if (!strcmp("ga_star", optarg)) {
					props.sm = GA_STAR;
					#ifndef CUDA_ENABLED
					throw CurvatureException("Cannot use GA_STAR. Reconfigure with --enable-cuda.\n");
					#endif
				} else
					throw CurvatureException("Invalid argument for parameter '--search-method'");
				break;
			case 'm':	//Manifold (region)
				if (!strcmp("R1", optarg))
					props.region = R1;
				else if (!strcmp("S1", optarg))
					props.region = S1;
				else if (!strcmp("S2", optarg))
					props.region = S2;
				else if (!strcmp("B2", optarg))
					props.region = BOLZA;
				else if (!strcmp("tree", optarg))
					props.region = BINARY_TREE;
				else if (!strcmp("lattice", optarg))
					props.region = Z2;
				else if (!strcmp("complete", optarg))
					props.region = COMPLETE;
				else if (!strcmp("T2", optarg))
					props.region = T2;
				else
					throw CurvatureException("Invalid argument for parameter '--manifold'");
				break;
			case 'N':	//Neighborhood prescription
				if (!strcmp("nearest", optarg))
					props.nh = NH_NEAREST;
				else if (!strcmp("delta", optarg))
					props.nh = NH_DELTA;
				else if (!strcmp("delta_discrete", optarg))
					props.nh = NH_DELTA_DISCRETE;
				else if (!strcmp("exact", optarg))
					props.nh = NH_EXACT;
				else
					throw CurvatureException("Invalid argument for parameter '--neighborhood'");
				break;
			case 'n':	//Number of nodes in the network
				props.N = atoi(optarg);
				if (props.N == 0)
					throw CurvatureException("Invalid argument for parameter '--nodes'");
				break;
			case 'p':	//Exponent associated with epsilon
				props.psi = atof(optarg);
				break;
			case 's':	//Random seed
				props.seed = atol(optarg);
				break;
			case 'S':	//Calculate the stretch
				props.calc_stretch = true;
				break;
			case 't':	//Target node separation
				if (!strcmp("epsilon", optarg))
					props.sep = SEP_EPSILON;
				else if (!strcmp("delta", optarg))
					props.sep = SEP_DELTA;
				else if (!strcmp("random", optarg))
					props.sep = SEP_RANDOM;
				else if (!strcmp("zero", optarg))
					props.sep = SEP_ZERO;
				else
					throw CurvatureException("Invalid argument for parameter '--target-separation'");
				break;
			case 'T':	//Test parameters
				props.test = true;
				break;
			case 'v':	//Visualize graph
				props.visualize = true;
				break;
			case 'w':	//Use weighted graph distances (manifold distances)
				props.weighted = true;
				break;
			default:
				throw CurvatureException("Flag not recognized!");
			}
		}
	} catch (const CurvatureException &e) {
		fprintf(stderr, "Exception in %s on line %d: %s\n", __FILE__, __LINE__, e.what());
		exit(1);
	}

	if (props.region == BINARY_TREE) {
		//Here the input N is taken to indicate the number
		//of layers past the root node to grow the binary tree
		assert (props.N <= 30);
		props.N = (1 << (props.N + 1)) - 1;
	} else if (props.region == Z2)
		props.N = props.N * props.N;

	//Initialize the RNGs
	if (props.seed == 12345L) {
		srand(time(NULL));
		props.seed = (long)time(NULL);
	}

	props.mrng.rng.engine().seed(props.seed);
	props.mrng.rng.distribution().reset();

	//Thermalize the RNG
	for (unsigned int i = 0; i < RNG_THERMAL; i++)
		props.mrng.rng();

	//The default search methods
	if (props.sm == SEARCH_DEFAULT) {
		if (props.use_gpu)
			props.sm = GA_STAR;
		else if (props.weighted)
			props.sm = HYBRID_ASTAR;
		else
			props.sm = BFS;
	}

	return props;
}

#endif
