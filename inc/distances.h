#ifndef OPERATIONS_H_
#define OPERATIONS_H_

#include "curvature.h"

/////////////////////////////
//(C) Will Cunningham 2019 //
//    Perimeter Institute  //
/////////////////////////////

//Used in Dijkstra's algorithm to identify the index of the minimum distance in 'dwork'
inline unsigned minDistance(const float * const dwork, const bool * const spwork, const unsigned N)
{
	float min = INF;
	unsigned idx = N;

	for (unsigned i = 0; i < N; i++)
		if (!spwork[i] && dwork[i] <= min)
			min = dwork[i], idx = i;

	return idx;
}

//Calculates Bolza distance for a pair (z0,z1) in the hyperbolic octagon
//Call this with all 49 generators and take the minimum
inline float dist(std::complex<float> &z0, std::complex<float> &z1)
{
	std::complex<float> numerator = z0 - z1;
	std::complex<float> denominator = std::conj(z0) * z1;
	denominator = std::complex<float>(1.0, 0.0) - denominator;
	std::complex<float> z = numerator / denominator;
	return 2.0 * atanh(sqrt(z.real() * z.real() + z.imag() * z.imag()));
}

//Used in 'linkNodes' to calculate the distance between a pair of nodes
inline float distance(const Coordinates * const crd, const Region region, const unsigned i, const unsigned j, const bool gpu)
{
	float dx, dy, d = 0.0;
	switch(region) {
	case R1:	//Periodic real line - use absolute value
		d = fabs(crd->points[0][i] - crd->points[0][j]);
		break;
	case T2:	//Torus, Euclidean distance
		if (gpu) {
			dx = 0.5 - fabs(0.5 - fabs(crd->h_points[i<<1] - crd->h_points[j<<1]));
			dy = 0.5 - fabs(0.5 - fabs(crd->h_points[(i<<1)+1] - crd->h_points[(j<<1)+1]));
		} else {
			dx = 0.5 - fabs(0.5 - fabs(crd->points[0][i] - crd->points[0][j]));
			dy = 0.5 - fabs(0.5 - fabs(crd->points[1][i] - crd->points[1][j]));
		}
		d = sqrt(dx * dx + dy * dy);
		break;
	case S2:	//Sphere, spherical law of cosines
		if (gpu)
			d = acos(cos((double)crd->h_points[i<<1]) * cos((double)crd->h_points[j<<1]) + sin((double)crd->h_points[i<<1]) * sin((double)crd->h_points[j<<1]) * cos((double)(crd->h_points[(i<<1)+1] - crd->h_points[(j<<1)+1])));
		else
			d = acos(cos((double)crd->points[0][i]) * cos((double)crd->points[0][j]) + sin((double)crd->points[0][i]) * sin((double)crd->points[0][j]) * cos((double)(crd->points[1][i] - crd->points[1][j])));
		break;
	case BOLZA:	//Bolza surface, minimize over hyperbolic distances
	{
		std::complex<float> z0, z1;
		if (gpu) {
			z0.real(crd->h_points[i<<1]);
			z0.imag(crd->h_points[(i<<1)+1]);
			z1.real(crd->h_points[j<<1]);
			z1.imag(crd->h_points[(j<<1)+1]);
		} else {
			z0.real(crd->points[0][i]);
			z0.imag(crd->points[1][i]);
			z1.real(crd->points[0][j]);
			z1.imag(crd->points[1][j]);
		}
		d = dist(z0, z1);

		//Iterate over the 48 generators (other than I)
		for (int m = 0; m < 48; m++) {
			std::complex<float> alpha(generators[m*4], generators[m*4+1]);
			std::complex<float> beta(generators[m*4+2], generators[m*4+3]);
			std::complex<float> gz1 = (alpha * z1 + beta) / (std::conj(beta) * z1 + std::conj(alpha));
			float dd = dist(z0, gz1);
			d = std::min(dd, d);
		}
		break;
	}
	default:
		assert (false);
		break;
	}

	return d;
}

#endif
