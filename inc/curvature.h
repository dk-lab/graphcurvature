/////////////////////////////
//(C) Will Cunningham 2018 //
//         DK Lab          //
// Northeastern University //
/////////////////////////////

#ifndef CURVATURE_H
#define CURVATURE_H

#include <algorithm>
#include <getopt.h>
#include <iomanip>
#include <numeric>
#include <queue>
#include <unordered_set>

#ifdef AVX2_ENABLED
#include <x86intrin.h>
#endif

#ifdef _OPENMP
  #include <omp.h>
#else
  #define omp_get_thread_num() 0
  #define omp_get_max_threads() 1
#endif

#include "config.h"
#include <fastmath/fastmath.h>
#include <fastmath/fastbitset.h>
#include <fastmath/mersenne.h>
#include <fastmath/stopwatch.h>
#include <fastmath/printcolor.h>
#include <hdf5.h>

#ifdef CUDA_ENABLED
#include <thrust/system/cuda/experimental/pinned_allocator.h>
#endif
#include <mosek.h>
#include "constants.h"

using namespace fastmath;

#ifdef CUDA_ENABLED
__host__ __device__ __forceinline__ bool operator>(const cuComplex &a, const cuComplex &b)
{
	return a.x > b.x || (a.x == b.x && a.y > b.y);;
}

template<typename T>
using pinned_vector = std::vector<T,thrust::system::cuda::experimental::pinned_allocator<T>>;
#else
template<typename T>
using pinned_vector = std::vector<T>;
#endif

template<typename T, typename priority_t>
struct PriorityQueue {
	typedef std::pair<priority_t, T> PQElement;
	std::priority_queue<PQElement, std::vector<PQElement>, std::greater<PQElement> > elements;

	inline bool empty() const
	{
		return elements.empty();
	}

	inline void put(T item, priority_t priority)
	{
		elements.emplace(priority, item);
	}

	T get()
	{
		T best_item = elements.top().second;
		elements.pop();
		return best_item;
	}

	inline void clear()
	{
		while (!elements.empty())
			elements.pop();
	}
};

template<typename _Tp, typename _Pair = std::pair<_Tp, unsigned>, typename _Sequence = std::vector<_Pair>, typename _Compare = std::greater<typename _Sequence::value_type> >
class FastPQ
{
private:
	_Sequence c;		//A vector of pairs
	_Compare comp;
	std::unordered_set<unsigned> s;

public:
	explicit FastPQ() { c.reserve(10000); s.reserve(10000); }

	explicit FastPQ(const _Compare& __x, const _Sequence& __s) : c(__s), comp(__x)
	{ std::make_heap(c.begin(), c.end(), comp); }

	inline bool empty() const { return c.empty(); }

	typename _Sequence::size_type size() const { return c.size(); }

	typename _Sequence::const_reference top() const { return c.front(); }

	void push(const typename _Sequence::value_type& __x)
	{
		c.push_back(__x);
		std::push_heap(c.begin(), c.end(), comp);
	}

	void emplace(const typename _Sequence::value_type& __x)
	{
		std::pair<std::unordered_set<unsigned>::iterator, bool> p = s.insert(__x.second);
		if (p.second)	//The item was not in the queue
			c.emplace_back(__x);
		else		//The item was in the queue, so just update its priority
			update_value(*(p.first), __x.first);
	}

	inline void sort() { std::sort_heap(c.begin(), c.end(), comp); }

	inline void refresh() { std::make_heap(c.begin(), c.end(), comp); }

	void pop()
	{
		std::pop_heap(c.begin(), c.end(), comp);
		s.erase(c.front().second);
		c.pop_back();
	}

	void clear()
	{
		c.clear(); c.shrink_to_fit();
		s.clear(); s.swap(s);
	}

	_Pair& get(const unsigned idx) { return c[idx]; }

	inline void update_value(const unsigned key, const _Tp value)
	{
		c[std::distance(c.begin(), std::find_if(c.begin(), c.end(), [&key](const _Pair& element) { return element.second == key; }))].first = value;
	}

	inline void update(const unsigned idx, const _Tp value) { c[idx].first = value; }

	void print() const
	{
		for (size_t i = 0; i < c.size(); i++)
			printf("[%u]: %f\n", c[i].second, c[i].first);
		printf("\n");
	}
};

struct __attribute__ ((aligned(16))) node_t {
	//cuComplex z;	//Coordinates
	unsigned id;	//Node ID
	unsigned k;	//Node Degree
	float f;	//Heuristic (cost from source + cost to destination)
	float g;	//Cost from source
};

struct __attribute__ ((aligned(8))) heap_t {
	float f;
	unsigned addr;
};

struct __attribute__ ((aligned(8))) sort_t {
	unsigned id;
	float g;
};

#ifdef CUDA_ENABLED
__host__ __device__ __forceinline__ bool operator<(const heap_t &a, const heap_t &b)
{
	return a.f < b.f;
}

__host__ __device__ __forceinline__ bool operator>(const heap_t &a, const heap_t &b)
{
	return a.f > b.f;
}

__host__ __device__ __forceinline__ bool operator<(const sort_t &a, const sort_t &b)
{
	return (a.id != b.id) ? (a.id < b.id) : (a.g < b.g);
	//return a.g < b.g;
}

__host__ __device__ __forceinline__ unsigned f2u(float f)
{
	union {
		float f;
		unsigned u;
	} un;
	un.f = f;
	return un.u ^ ((un.u >> 31) | 0x80000000);
}

__host__ __device__ __forceinline__ float u2f(unsigned u)
{
	union {
		float f;
		unsigned u;
	} un;
	un.u = u ^ ((~u >> 31) | 0x80000000);
	return un.f;
}
#endif

inline int div_up(int x, int y)
{
	return (x - 1) / y + 1;
}

enum Region {
	R1,	//Real line between [-0.5,0.5]
	S1,	//Circle (no coordinates)
	S2,	//2-sphere
	BOLZA,	//Bolza surface (compact hyperbolic)
	BINARY_TREE,	//Binary tree (no coordinates)
	Z2,	//2d lattice (no coordinates)
	COMPLETE,	//Complete graph (no coordinates)
	T2	//2-torus on [-1,1]x[-1,1]
};

static constexpr const char *regions[] = { "R1", "S1", "S2", "BOLZA", "BINARY TREE", "Z2", "COMPLETE", "T2" };

enum SearchMethod {
	BFS,		//Breadth first search
	DIJKSTRA,
	ASTAR,
	HYBRID_ASTAR,	//A* source-to-multi-destination variant
	GA_STAR,	//GPU A*, see dl.acm.org/citation.cfm?id=2887007.2887180
	SEARCH_DEFAULT	//User does not specify method
};

static constexpr const char *searchmethods[] = { "BREADTH FIRST SEARCH", "DIJKSTRA", "A*", "HYBRID A*", "GA*", "UNDETERMINED" };

enum Separation {
	SEP_EPSILON,
	SEP_DELTA,
	SEP_RANDOM,
	SEP_ZERO
};

static constexpr const char *separations[] = { "EPSILON", "DELTA", "RANDOM", "ZERO" };

enum Neighborhood {
	NH_NEAREST,
	NH_DELTA,
	NH_DELTA_DISCRETE,
	NH_EXACT
};

static constexpr const char *neighborhoods[] = { "NEAREST", "DELTA", "DISCRETE DELTA", "EXACT" };

struct Coordinates {
	Coordinates(unsigned _dim, bool _gpu) : h_points(NULL), dim(_dim), gpu(_gpu) {
		if (gpu)
			points = NULL;
		else {
			points = new float*[dim];
			for (unsigned i = 0; i < dim; i++)
				points[i] = NULL;
		}
	}
	virtual ~Coordinates() { if (!gpu) { delete [] this->points; this->points = NULL; } }
	
	float **points;
	float *h_points;
	unsigned dim;
	bool gpu;
};

struct Nodes {
	Nodes() : crd(NULL), k(NULL), cc_id(NULL)  {}

	Coordinates *crd;
	unsigned int *k;	//Degrees
	int *cc_id;		//Connected Component ID
};

struct Properties {
	Properties() : mrng(MersenneRNG()), region(R1), sm(SEARCH_DEFAULT), sep(SEP_DELTA), nh(NH_NEAREST), N(100), alpha(0.333), beta(0.333), chi(1.0/6.0), psi(0.5), seed(12345L), graph_id(0), classic(false), weighted(false), calc_stretch(false), use_gpu(false), verbose(false), visualize(false), test(false) {}

	MersenneRNG mrng;
	Region region;
	SearchMethod sm;
	Separation sep;
	Neighborhood nh;
	unsigned int N;
	float alpha;	//Coefficient for delta
	float beta;	//Coefficient for epsilon
	float chi;	//Exponent associated with delta
	float psi;	//Exponent associated with epsilon
	long seed;
	unsigned long graph_id;

	bool classic;	//Classic Ollivier curvature, epsilon=delta
	bool weighted;	//Use weighted edges to calculate distances
	bool calc_stretch;
	bool use_gpu;
	bool verbose;
	bool visualize;
	bool test;
};

struct Observables {
	Observables() : rmax(0.0), k_avg(0.0), wasserstein(0.0), ollivier(0.0), ricci(0.0), measures(NULL), distances(NULL), k_max(0), stretch(0.0) {}

	double rmax;
	double k_avg;
	double wasserstein;
	double ollivier;
	double ricci;
	float *measures;
	double *distances;
	unsigned k_max;
	pinned_vector<float> h_measures;	//Sparse representation
	double stretch;
};

struct Network {
	Network() : nodes(Nodes()), props(Properties()), obs(Observables()) {}
	Network(Properties _props) : nodes(Nodes()), props(_props), obs(Observables()) {}

	Bitvector adj;
	Nodes nodes;
	Properties props;
	Observables obs;

	//Sparse data structures
	pinned_vector<unsigned> h_links;
	pinned_vector<uint64_t> h_linkidx;
};

struct Performance {
	Performance() : sCurvature(Stopwatch()) {}

	Stopwatch sCurvature;
};

class CurvatureException : public std::exception
{
public:
	CurvatureException() : msg("Unknown Error!") {}
	explicit CurvatureException(char const * _msg) : msg(_msg) {}
	virtual ~CurvatureException() throw () {}
	virtual const char * what() const throw () { return msg; }

protected:
	char const * msg;
};

#include "resources.h"
#include "subroutines.h"

bool initializeNetwork(Network * const network, Resources * const res, Performance * const perf, CuResources * const cu);

double wasserstein(const double * const distances, std::vector<unsigned> &Xnbr, std::vector<unsigned> &Ynbr, unsigned int X, unsigned int Y, Resources * const res);

void destroyNetwork(Network * const network, Resources * const res, CuResources * const cu);

#endif
