#ifndef LINK_KERNELS_CUH_
#define LINK_KERNELS_CUH_

#include "kernels/distance_kernels.cuh"

/////////////////////////////
//(C) Will Cunningham 2019 //
//    Perimeter Institute  //
/////////////////////////////

//Bolza kernel, 128 threads per block
__global__ void bolza_v1(const cuComplex * __restrict__ g_z0, const cuComplex * __restrict__ g_z1, unsigned *k, unsigned *edges, float *measures, const float4 * __restrict__ generators, const size_t size0, const size_t size1, const float delta)
{
	unsigned tid = threadIdx.y;
	unsigned wid = tid & 31;
	unsigned i = blockIdx.x;
	unsigned j = blockIdx.y << 2;

	__shared__ float4 shr_gen[48];
	__shared__ float shr_d[4];
	__shared__ unsigned shr_edge[4];
	__shared__ cuComplex shr_z0, shr_z1[4];

	cuComplex z0, z1;
	float d = INF;

	//i -> column
	if (i < size0) {
		if (!tid)
			shr_z0 = g_z0[i];
		else if (tid >= 32 && tid < 80)
			shr_gen[tid-32] = generators[tid-32];
		__syncthreads();
	} 
	z0 = shr_z0;

	//j+(tid>>5) -> row
	if (j + (tid>>5) < size1)
		z1 = g_z1[j+(tid>>5)];
	if (!wid)
		shr_z1[tid>>5] = z1;

	bool valid = (i < size0) && (j + (tid >> 5) < size1);

	//Calculate hyperbolic distance (first 32 calculations)
	if (valid)
		d = distance<BOLZA>(z0, map_point(shr_gen[wid], z1));

	//Place minimum (among first 32 calculations) in warp lane 0
	#pragma unroll
	for (unsigned offset = 1; offset < 32; offset <<= 1)
		d = fminf(__shfl_xor_sync(0xffffffff, d, offset), d);

	//Place value from warp lane 0 into shared memory
	if (!wid)
		shr_d[tid>>5] = d;
	__syncthreads();

	if (tid < 64) {
		//Calculate next 16 distances for each pair
		if (i < size0 && j + (tid >> 4) < size1)
			//Threads 0-15 use z1 already in those registers: {0-15} -> {0-15}
			//Threads 16-31 use z1 from threads 32-63: {32-47} -> {16-31}
			//Threads 32-47 use z1 from threads 64-95: {64-79} -> {32-47}
			//Threads 48-63 use z1 from threads 96-127: {96-111} -> {48-63}
			d = distance<BOLZA>(z0, map_point(shr_gen[(wid&15)+32], shr_z1[tid>>4]));

		//Minimum distances (among next 16 calculations) are placed in warp lanes 0, 16 in warps 0, 1
		#pragma unroll
		for (unsigned offset = 1; offset < 16; offset <<= 1)
			d = fminf(__shfl_xor_sync(0x0000ffff, d, offset), d);
	}
	__syncthreads();

	if (tid < 64 && !(wid & 15)) //tid = {0, 16, 32, 48}
		shr_d[tid>>4] = fminf(shr_d[tid>>4], d);
	__syncthreads();

	//Calculate final distance, write results to global memory
	if (!wid) {
		if (valid)
			d = fminf(distance<BOLZA>(z0, z1), shr_d[tid>>5]);
		bool edge_exists = d > TOL && d <= delta;
		shr_edge[tid>>5] = (uint8_t)edge_exists << (tid >> 2);
		atomicAdd(&k[j+(tid>>5)], edge_exists);
		measures[i*4*gridDim.y+j+(tid>>5)] = (unsigned)valid * d + (unsigned)!valid * INF;
	}
	__syncthreads();

	if (!tid) {
		unsigned edge = shr_edge[0] | shr_edge[1] | shr_edge[2] | shr_edge[3];
		edges[blockIdx.x*gridDim.y+blockIdx.y] = edge;
	}
}

//GPU linking kernel
template<Region REG, unsigned NT, unsigned LT = 1>
__global__ void link(const float2 * __restrict__ g_z0, const float2 * __restrict__ g_z1, unsigned *degree, uint8_t *edges, float *measures, const size_t size0, const size_t size1, const float delta)
{
	__shared__ float2 shr_z0[LT];
	float d[LT];
	bool edge[LT];
	float2 z0, z1;

	unsigned tid = threadIdx.y;
	unsigned i = blockIdx.x;	//Column
	unsigned j = blockDim.y * blockIdx.y + threadIdx.y;	//Row

	//Read from global to shared (cache) memory
	if (!tid) {
		#pragma unroll
		for (unsigned k = 0; k < LT; k++) {
			unsigned idx = i * LT + k;
			if (idx < size0)
				shr_z0[k] = g_z0[idx];
		}
	}
	__syncthreads();

	//Calculate the node-pair distance for z0,z1
	//We consider LT pairs per thread
	#pragma unroll
	for (unsigned k = 0; k < LT; k++) {
		unsigned idx = i * LT + k;
		bool valid = idx < size0 && j < size1;
		if (valid) {
			z0 = shr_z0[k];
			z1 = g_z1[j];
			d[k] = distance<REG>(z0, z1);
		} 
		edge[k] = valid && d[k] > TOL && d[k] <= delta;
	}
	__syncthreads();

	//Write results to global memory
	unsigned n = 0;
	#pragma unroll
	for (unsigned k = 0; k < LT; k++) {
		unsigned idx = i * LT + k;
		bool valid = idx < size0 && j < size1;
		n += (unsigned)edge[k];
		edges[idx*blockDim.y*gridDim.y+j] = (uint8_t)edge[k];
		measures[idx*blockDim.y*gridDim.y+j] = (unsigned)valid * d[k] + (unsigned)!valid * INF;
	}
	atomicAdd(&degree[j], n);
}

#endif
