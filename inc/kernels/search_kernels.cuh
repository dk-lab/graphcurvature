#ifndef SEARCH_KERNELS_CUH_
#define SEARCH_KERNELS_CUH_

#include "kernels/distance_kernels.cuh"

/////////////////////////////
//(C) Will Cunningham 2019 //
//    Perimeter Institute  //
/////////////////////////////

//All of the following subroutines are GPU methods for the GA* algorithm

//Initialize GPU data structures
template<Region region, bool weighted>
__global__ void initGAstar(node_t *nodes, unsigned *hash, heap_t *openlist, int *heapsize, const cuComplex * __restrict__ z, const float4 * __restrict__ generators, const unsigned * __restrict__ degrees, const unsigned source, const unsigned sink, const float epsilon)
{
	node_t node;
	cuComplex z0 = z[max(source, sink)];
	cuComplex z1 = z[min(source, sink)];
	node.id = source;
	node.k = degrees[source];
	//Heuristic is simply the geodesic distance between z0,z1
	node.f = distance_all<region>(z0, z1, generators);
	if (!weighted)
		node.f = epsilon * ceilf(node.f / epsilon);
	//Current 'frontier' of the search is at the source, so 'g' is zero
	node.g = 0.0;

	//The only node in the heap initially is the source node
	heap_t heap;
	heap.f = node.f;
	heap.addr = 0;	//Assigned to the first priority queue

	nodes[0] = node;
	openlist[0] = heap;

	hash[source] = 0;
	heapsize[0] = 1;
}

//Extract nodes in the openlist (frontier)
//Each thread does 'VT' nodes serially
template<int NB, int NT, int HC, bool weighted>
__global__ void extract_v2(const node_t * __restrict__ nodes, const unsigned * __restrict__ links, const uint64_t * __restrict__ linkidx, const float * __restrict__ measures, heap_t *openlist, int *heapsize, unsigned *mindist, sort_t *sortlist, int *sortlistsize, int *heapindex, int *heapinsertsize, heap_t *optimalnodes, int *optimalsize, const unsigned N, const float epsilon, const unsigned sink)
{
	__shared__ unsigned s_mindist;
	__shared__ int s_sortlistsize;
	__shared__ int s_sortlistbase;
	__shared__ int s_maxrounds[NT/32];

	int gid = GLOBAL_ID;
	int tid = THREAD_ID;
	int wid = tid & 31;
	if (!tid) {
		s_mindist = UINT32_MAX;
		s_sortlistsize = 0;
		s_sortlistbase = 0;
	}
	__syncthreads();

	heap_t *heap = openlist + HC * gid - 1;
	heap_t extracted;
	int heap_size = heapsize[gid];
	int sortlistcount = 0;
	int rounds = 0;
	sort_t sort_list[MAX_SMALL];
	node_t node;

	if (!!heap_size) {
		extracted = heap[1];

		heap_t nowvalue = heap[heap_size--];
		int now = 1;
		int next;
		while ((next = now << 1) <= heap_size) {
			heap_t nextvalue = heap[next];
			heap_t nextvalue2 = heap[next+1];
			bool inc = (next + 1 <= heap_size) && (nextvalue2 < nextvalue);
			if (inc) {
				next++;
				nextvalue = nextvalue2;
			}

			if (nextvalue < nowvalue) {
				heap[now] = nextvalue;
				now = next;
			} else
				break;
		}
		heap[now] = nowvalue;

		//Check if the extracted distance is smaller than the minimum
		atomicMin(&s_mindist, f2u(extracted.f));
		node = nodes[extracted.addr];
		rounds = (int)ceilf((float)node.k / MAX_SMALL);

		//The sink has been reached
		if (node.id == sink) {
			int index = atomicAdd(optimalsize, 1);
			optimalnodes[index] = extracted;
		}
	}
	__syncthreads();
	heapsize[gid] = heap_size;

	//Calculate how many rounds are needed to assign nodes to sortlists
	int w_maxrounds = rounds;
	#pragma unroll
	for (unsigned offset = 1; offset < 32; offset <<= 1)
		w_maxrounds = max(__shfl_xor_sync(0xffffffff, w_maxrounds, offset), w_maxrounds);
	if (!wid && !!tid)
		s_maxrounds[tid>>5] = w_maxrounds;
	__syncthreads();
	int maxrounds;
	if (!tid) {
		maxrounds = w_maxrounds;
		for (int i = 1; i < NT / 32; i++)
			maxrounds = max(maxrounds, s_maxrounds[i]);
		s_maxrounds[0] = maxrounds;
	}
	__syncthreads();

	maxrounds = s_maxrounds[0];
	uint64_t linkoffset;
	if (rounds)
		linkoffset = linkidx[node.id];

	//Assign nodes to sortlists
	for (int i = 0; i < maxrounds; i++) {
		int offset = i * MAX_SMALL;
		sortlistcount = 0;
		if (!tid)
			s_sortlistsize = 0;
		__syncthreads();

		for (int j = 0; j < MAX_SMALL && i < rounds; j++) {
			int nbr = offset + j;
			if (nbr >= node.k) continue;
			unsigned id = links[linkoffset+nbr];
			sort_list[j].id = id;
			sort_list[j].g = node.g + (weighted ? measures[linkoffset+nbr] : epsilon);
			sortlistcount++;
		}

		int sortlistindex = atomicAdd(&s_sortlistsize, sortlistcount);
		__syncthreads();
		if (!tid)
			s_sortlistbase = atomicAdd(sortlistsize, s_sortlistsize);
		__syncthreads();
		sortlistindex += s_sortlistbase;

		for (int j = 0; j < MAX_SMALL && i < rounds; j++) {
			int nbr = offset + j;
			if (nbr >= node.k) continue;
			sortlist[sortlistindex++] = sort_list[j];
		}
	}

	if (!tid)
		atomicMin(mindist, s_mindist);

	//Save heap indices
	if (!gid) {
		int newheapidx = *heapindex + *heapinsertsize;
		*heapindex = newheapidx % (NB * NT);
		*heapinsertsize = 0;
	}
}

//Assign nodes to sortlists
template<int NT>
__global__ void assign(const sort_t * __restrict__ sortlist, const int sortlistsize, sort_t *sortlist2, int *sortlistsize2)
{
	__shared__ unsigned s_idlist[NT+1];
	__shared__ unsigned s_sortlistcount;
	__shared__ unsigned s_sortlistbase;

	int tid = THREAD_ID;
	int gid = GLOBAL_ID;

	bool working = false;
	sort_t sort;

	if (!tid) {
		s_sortlistcount = 0;
		if (!!gid)
			s_idlist[0] = sortlist[gid-1].id;
	}

	if (gid < sortlistsize) {
		working = true;
		sort = sortlist[gid];
		s_idlist[tid+1] = sort.id;
	}
	__syncthreads();

	working &= (!gid || s_idlist[tid] != s_idlist[tid+1]);

	int index;
	if (working)
		index = atomicAdd(&s_sortlistcount, 1);
	__syncthreads();
	
	if (!tid)
		s_sortlistbase = atomicAdd(sortlistsize2, s_sortlistcount);
	__syncthreads();
	
	if (working)
		sortlist2[s_sortlistbase+index] = sort;
}

//Remove duplicates
template<Region region, int NT, bool weighted>
__global__ void deduplicate(node_t *nodes, int *nodesize, unsigned *hash, const sort_t * __restrict__ sortlist, const int sortlistsize, heap_t *heapinsertlist, int *heapinsertsize, const cuComplex * __restrict__ z, const float4 * __restrict__ generators, const unsigned * __restrict__ degrees, const float epsilon, const unsigned sink)
{
	int tid = THREAD_ID;
	int gid = GLOBAL_ID;
	bool working = gid < sortlistsize;

	__shared__ int s_nodeinsertcount;
	__shared__ int s_nodeinsertbase;
	__shared__ int s_heapinsertcount;
	__shared__ int s_heapinsertbase;

	if (!tid) {
		s_nodeinsertcount = 0;
		s_heapinsertcount = 0;
	}
	__syncthreads();

	node_t node;
	bool insert = true;
	bool found = true;
	unsigned nodeindex, heapindex, addr;

	if (working) {
		node.id = sortlist[gid].id;
		node.g = sortlist[gid].g;
		node.k = degrees[node.id];
		node.f = node.g;
		if (node.id != sink) {
			float h = distance_all<region>(z[max(node.id, sink)], z[min(node.id, sink)], generators);
			if (!weighted)
				h = epsilon * ceil(h / epsilon);
			node.f += h;
		}

		addr = hash[node.id];
		found = (addr != UINT32_MAX);
		if (found) {
			if (node.f < nodes[addr].f)
				nodes[addr] = node;
			else
				insert = false;
		} else
			nodeindex = atomicAdd(&s_nodeinsertcount, 1);

		if (insert)
			heapindex = atomicAdd(&s_heapinsertcount, 1);
	}
	__syncthreads();

	if (!tid) {
		s_nodeinsertbase = atomicAdd(nodesize, s_nodeinsertcount);
		s_heapinsertbase = atomicAdd(heapinsertsize, s_heapinsertcount);
	}
	__syncthreads();

	if (working && !found) {
		addr = s_nodeinsertbase + nodeindex;
		hash[node.id] = addr;
		nodes[addr] = node;
	}

	if (working && insert) {
		unsigned index = s_heapinsertbase + heapindex;
		heapinsertlist[index].f = node.f;
		heapinsertlist[index].addr = addr;
	}
}

//Heap insert
//Nodes are re-added to priority queues
template<int NB, int NT, int HC>
__global__ void heapinsert(heap_t *openlist, int *heapsize, const int * __restrict__ heapindex, const heap_t * __restrict__ heapinsertlist, const int * __restrict__ heapinsertsize, int *sortlistsize, int *sortlistsize2, unsigned *mindist, int *optimalsize)
{
	int gid = GLOBAL_ID;
	
	int heap_insert_size = *heapinsertsize;
	int heap_index = *heapindex + gid;
	if (heap_index >= NB * NT)
		heap_index -= NB * NT;

	int heap_size = heapsize[heap_index];
	heap_t *heap = openlist + HC * heap_index - 1;

	for (int i = gid; i < heap_insert_size; i += NB * NT) {
		heap_t value = heapinsertlist[i];
		int now = ++heap_size;

		while (now > 1) {
			int next = now >> 1;
			heap_t next_value = heap[next];
			if (value < next_value) {
				heap[now] = next_value;
				now = next;
			} else
				break;
		}
		heap[now] = value;
	}
	
	heapsize[heap_index] = heap_size;
	//Variables are reset for the next round
	if (!gid) {
		*sortlistsize = 0;
		*sortlistsize2 = 0;
		*mindist = UINT32_MAX;
		*optimalsize = 0;
	}
}

//Heuristic functions are updated when a new sink is used
template<Region region, int NB, int NT, int HC, bool weighted>
__global__ void updateHeuristics(node_t *nodes, const int * __restrict__ nodesize, unsigned *hash, heap_t *openlist, const int * __restrict__ heapsize, const int * __restrict__ heapindex, heap_t *heapinsertlist, int *heapinsertsize, unsigned *mindist, heap_t *optimalnodes, int *optimalsize, int os, const cuComplex * __restrict__ z, const float4 * __restrict__ generators, const float epsilon, const unsigned sink)
{
	int gid = GLOBAL_ID;
	int tid = THREAD_ID;

	__shared__ unsigned s_mindist;
	if (!tid)
		s_mindist = UINT32_MAX;
	__syncthreads();

	while (gid < *nodesize) {
		node_t node = nodes[gid];
		node.f = node.g;
		if (node.id != sink) {
			float h = distance_all<region>(z[max(node.id, sink)], z[min(node.id, sink)], generators);
			if (!weighted)
				h = epsilon * ceil(h / epsilon);
			node.f += h;
		}
		atomicMin(&s_mindist, f2u(node.f));
		nodes[gid] = node;
		if (node.id == sink) {
			int index = atomicAdd(optimalsize, 1);
			optimalnodes[index].f = node.f;
			optimalnodes[index].addr = gid;
		}
		gid += NB * NT;
	}
	__syncthreads();
	gid = GLOBAL_ID;

	if (!tid)
		atomicMin(mindist, s_mindist);

	if (blockIdx.x < NUM_BLOCK) {
		int heap_index = *heapindex + gid;
		if (heap_index >= NUM_BLOCK * NT)
			heap_index -= NUM_BLOCK * NT;
		int heap_size = heapsize[heap_index];
		heap_t *heap = openlist + HC * heap_index;
		for (int i = 0; i < heap_size; i++)
			heap[i].f = nodes[heap[i].addr].f;
	}
}

#endif
