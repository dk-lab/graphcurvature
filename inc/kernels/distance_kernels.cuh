#ifndef DISTANCE_KERNELS_CUH_
#define DISTANCE_KERNELS_CUH_

/////////////////////////////
//(C) Will Cunningham 2019 //
//    Perimeter Institute  //
/////////////////////////////

//Apply the Bolza generator to the point 'z'
__device__ __forceinline__ cuComplex map_point(const float4 &generator, const cuComplex &z)
{
	cuComplex alpha = make_cuComplex(generator.x, generator.y);
	cuComplex beta = make_cuComplex(generator.z, generator.w);

	return cuCdivf(cuCaddf(cuCmulf(alpha, z), beta), cuCaddf(cuCmulf(cuConjf(beta), z), cuConjf(alpha)));
}

//Calculate distance between a pair of points
template<Region region>
__device__ __forceinline__ float distance(const cuComplex &z0, const cuComplex &z1)
{
	float d = INF;

	if (region == T2) {
		double dx = 0.5 - fabs(0.5 - fabs((double)(z0.x - z1.x)));
		double dy = 0.5 - fabs(0.5 - fabs((double)(z0.y - z1.y)));
		d = sqrtf(dx * dx + dy * dy);
	} else if (region == S2) {
		double c0, s0, c1, s1;
		sincos((double)z0.x, &s0, &c0);
		sincos((double)z1.x, &s1, &c1);
		d = (float)acos(c0 * c1 + s0 * s1 * cos((double)(z0.y - z1.y)));
	} else if (region == BOLZA) {
		cuComplex numerator = cuCsubf(z0, z1);
		cuComplex denominator = cuCmulf(cuConjf(z0), z1);
		denominator.x = 1.0 - denominator.x;
		denominator.y = -denominator.y;
		cuComplex z = cuCdivf(numerator, denominator);
		d = 2.0 * atanh(hypotf(z.x, z.y));
	}

	return d;
}

//Calculate the shortest-path between a pair of points
//on the Bolza surface, given the 49 generators
template<Region region>
__device__ __forceinline__ float distance_all(const cuComplex &z0, const cuComplex &z1, const float4 * __restrict__ generators)
{
	float d = distance<region>(z0, z1);
	if (region == BOLZA) {
		#pragma unroll
		for (unsigned i = 0; i < 48; i++)
			d = fminf(d, distance<BOLZA>(z0, map_point(generators[i], z1)));
	}
	return d;
}

#endif
