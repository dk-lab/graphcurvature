#ifndef SUBROUTINES_H_
#define SUBROUTINES_H_

#include "search.h"

/////////////////////////////
//(C) Will Cunningham 2018 //
//         DK Lab          //
// Northeastern University //
/////////////////////////////

bool generateNodes(Network *network, float &delta, float &epsilon);

bool linkNodes(Network *network, CuResources * const cu, const float delta);

bool linkNodesGPU_v2(Network *network, Resources * const res, CuResources * const cu, const float delta);

void measureStretch_v2(Network *network, Resources *res, const float epsilon, const uint64_t npairs);

bool measureDistances(Network *network, Resources *res, CuResources *cu, const float delta, const float epsilon, std::vector<unsigned> &Xnbr, std::vector<unsigned> &Ynbr, const unsigned X, const unsigned Y, float &dist_xy);

void measureConnectedComponents(Nodes &nodes, const Bitvector &adj, const unsigned N, Resources * const res);

void get_ball_exact(Network *network, std::vector<unsigned> &nbr, const float delta, const unsigned root);

void get_ball(Network *network, Resources *res, std::vector<unsigned> &nbr, const float delta, const float epsilon, const unsigned root, const bool weighted);

void quicksort(std::vector<unsigned> &links0, pinned_vector<unsigned> &links1, pinned_vector<float> &measures, int64_t low, int64_t high);

bool write_to_h5(hid_t &handle, void *data, hsize_t dim, hsize_t *shape, hsize_t *maxshape, hsize_t *chunk_size, hid_t datatype, const char *dset_name, bool create, bool append);

bool save_to_h5(Network * const network);

void write_constants(Network * const network);

inline void readDegrees(unsigned * const &degrees, const unsigned * const h_k, const size_t &offset, const size_t &size)
{
	for (unsigned i = 0; i < size; i++)
		degrees[offset+i] += h_k[i];
}

inline void readEdges(Bitvector &adj, uint8_t * const h_edges, const unsigned x, const unsigned y, const size_t blocks_per_grid_x, const size_t size0, const size_t size1)
{
	for (size_t i = 0; i < size0; i++)
		for (size_t j = 0; j < size1; j++)
			if (h_edges[i*blocks_per_grid_x+j])
				adj[y*blocks_per_grid_x+j].set(x*blocks_per_grid_x+i);
}

inline uint64_t index_map(unsigned i, unsigned j, unsigned N)
{
	if (i > j)
		std::swap(i, j);

	unsigned nm = N - (N & 1);
	unsigned np = N + (N & 1);
	unsigned do_map = (unsigned)(i >= (nm >> 1));
	i -= do_map * (((i - (nm >> 1)) << 1) + 1);
	j -= do_map * ((j - (nm >> 1)) << 1);

	return (uint64_t)i * (np - 1) + j - !(N & 1);
}

inline void readMeasures(float * const measures, float * const h_measures, const unsigned N, const unsigned x, const unsigned y, const size_t blocks_per_grid_x, const size_t size0, const size_t size1)
{
	for (size_t i = 0; i < size0; i++)
		for (size_t j = 0; j < size1; j++)
			if (y*blocks_per_grid_x+j != x*blocks_per_grid_x+i)
				measures[index_map(y*blocks_per_grid_x+j, x*blocks_per_grid_x+i, N)] = h_measures[i*blocks_per_grid_x+j];
}

inline void readData(Bitvector &adj, std::vector<unsigned> &links0, pinned_vector<unsigned> &links1, uint8_t * const h_edges, float * const measures, pinned_vector<float> &s_measures, float * const h_measures, const unsigned N, const unsigned x, const unsigned y, const size_t blocks_per_grid_x, const size_t size0, const size_t size1, bool use_sparse)
{
	uint64_t map;
	size_t idx0, idx1, loc_idx;
	for (size_t i = 0; i < size0; i++) {
		idx0 = x * blocks_per_grid_x + i;
		for (size_t j = 0; j < size1; j++) {
			idx1 = y * blocks_per_grid_x + j;
			loc_idx = i * blocks_per_grid_x + j;
			if (idx0 > idx1) {
				map = index_map(idx0, idx1, N);
				measures[map] = h_measures[loc_idx];
				if (h_edges[loc_idx]) {
					adj[idx1].set(idx0);
					adj[idx0].set(idx1);
					if (use_sparse) {
						links0.push_back(idx0);
						links1.push_back(idx1);
						s_measures.push_back(measures[map]);

						links0.push_back(idx1);
						links1.push_back(idx0);
						s_measures.push_back(measures[map]);
					}
				}
			}
		}
	}
}

inline void MSKAPI printstr(void *handle, const char str[])
{
	printf("%s", str);
}

inline void swap(std::vector<unsigned> &links0, pinned_vector<unsigned> &links1, pinned_vector<float> &measures, const int64_t i, const int64_t j)
{
	std::swap(links0[i], links0[j]);
	std::swap(links1[i], links1[j]);
	std::swap(measures[i], measures[j]);
}

inline float expected_avg_degree(const Region region, const unsigned N, const float epsilon) {
	float k = 0.0, volume = 0.0;
	switch (region) {
	case T2:
		volume = 1;
		k = M_PI * N * POW2(epsilon) / volume;
		break;
	case S2:
		volume = 4 * M_PI;
		k = 2.0 * M_PI * (1.0 - cos(epsilon)) * N / volume;
		break;
	case BOLZA:
		volume = 4 * M_PI;
		k = 2.0 * M_PI * (cosh(epsilon) - 1.0) * N / volume;
		break;
	default:
		k = std::numeric_limits<float>::quiet_NaN();
	}
	return k;
}

#endif
