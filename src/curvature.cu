/////////////////////////////
//(C) Will Cunningham 2018 //
//         DK Lab          //
// Northeastern University //
/////////////////////////////

#include "curvature.h"
#include "parse.h"

//Main entry point
int main(int argc, char **argv)
{
	//Allocate resources for memory management
	Resources res = Resources();
	//Allocate CUDA resources
	CuResources cu = CuResources();
	//Stopwatches
	Performance perf = Performance();

	stopwatchStart(&perf.sCurvature);

	//Allocate main data structure for the model
	Network network = Network(parseArgs(argc, argv));
	//Write the input parameters to file (parameters.log)
	write_constants(&network);

	//Print CPU info
	int e_start = printStart((const char**)argv, 0);
	bool success = false;

	//Connect to GPU(s)
	#ifdef CUDA_ENABLED
	if (network.props.use_gpu)
		connectToGPU(&cu, argc, argv, 0);
	#endif

	//Allocate memory, initialize system
	//Most of the work is done in this function
	//because this is a single-purpose toolkit
	if (!initializeNetwork(&network, &res, &perf, &cu)) goto Exit;

	//Free memory
	destroyNetwork(&network, &res, &cu);

	//Disconnect from GPU(s)
	#ifdef CUDA_ENABLED
	if (network.props.use_gpu)
		for (unsigned i = 0; i < MAX_GPUS; i++)
			cuCtxDestroy(cu.cuContext[i]);
	#endif

	//Check to make sure memory has been properly freed
	success = !res.hostMemUsed && !res.devMemUsed;
	if (!success) {
		printf("WARNING: Memory leak detected!\n");
	}

	Exit:
	stopwatchStop(&perf.sCurvature);

	printFinish((const char**)argv, e_start, 0, success ? PASSED : FAILED);

	printf("Time: %5.6f sec\n", perf.sCurvature.elapsedTime);
	printf("PROGRAM COMPLETED\n\n");
	fflush(stdout);
	
	return 0;
}

//This is the main method that contains the high-level work
//Lower-level subroutines are called from this function
bool initializeNetwork(Network * const network, Resources * const res, Performance * const perf, CuResources * const cu)
{
	#ifdef _OPENMP
	printf("\n\t[ *** OPENMP MODULE ACTIVE *** ]\n");
	printf("\t\tUsing [%d] threads.\n", omp_get_max_threads());
	#endif

	#ifdef AVX2_ENABLED
	printf("\n\t[ ***  AVX2 MODULE ACTIVE  *** ]\n");
	#endif
	fflush(stdout);

	//If multiple GPUs are being used, push one context on the stack
	#ifdef CUDA_ENABLED
	if (MAX_GPUS > 1 && network->props.use_gpu)
		cuCtxPushCurrent(cu->cuContext[0]);
	#endif

	///////////////////////
	// MEMORY ALLOCATION //
	///////////////////////

	//Allocate the coordinates and degree vectors
	unsigned int dim = (network->props.region == R1) ? 1 : 2;
	network->nodes.crd = new Coordinates(dim, network->props.use_gpu && network->props.N >= GPU_MIN);
	if (!network->props.use_gpu) {
		for (unsigned int i = 0; i < dim; i++)
			network->nodes.crd->points[i] = MALLOC<float>(network->props.N, res);
		network->nodes.k = MALLOC<unsigned, true>(network->props.N, res);
	} else {
		network->nodes.crd->h_points = MALLOC<float, false, true>(dim * network->props.N, res);
		network->nodes.k = MALLOC<unsigned, true, true>(network->props.N, res);
	}

	//Allocate the adjacency matrix
	//It is stored in compressed-bit format as a Bitvector (defined in fastbitset.h)
	//which is a vector, and each entry is a FastBitset that represents a row
	//of the adjacency matrix
	network->adj.reserve(network->props.N);
	for (unsigned int i = 0; i < network->props.N; i++) {
		FastBitset fb = FastBitset(network->props.N);
		network->adj.push_back(fb);
		res->hostMemUsed += sizeof(BlockType) * fb.getNumBlocks();
	}

	//Number of node pairs in a graph with N nodes
	uint64_t npairs = (uint64_t)network->props.N * (network->props.N - 1) / 2;

	//Allocate graph measures
	//These contain weighted distances for nearest neighbors in the graph
	network->obs.measures = MALLOC<float>(npairs, res);
	//Initialized to INF (infinity)
	std::fill(network->obs.measures, network->obs.measures + npairs, INF);

	//If we use the GA* algorithm for shortest paths,
	//we need to allocate some extra data structures to make
	//copying to-and-from the GPU more efficient
	if (network->props.use_gpu && network->props.sm == GA_STAR) {
		//These are sparse vectors which hold the same data as 'adj'
		//They used pinned memory on the GPU
		network->h_links.reserve(npairs / 10);
		network->h_linkidx.resize(network->props.N);
		network->obs.h_measures.reserve(npairs / 10);
	}

	//If multiple GPUs are being used, pop the current context off the stack
	#ifdef CUDA_ENABLED
	if (MAX_GPUS > 1 && network->props.use_gpu)
		cuCtxPopCurrent(&cu->cuContext[0]);
	#endif

	memoryCheckpoint(res);
	printMemUsed("for Network Construction", res->hostMemUsed, res->devMemUsed, 0);

	/////////////////////
	// NODE SPRINKLING //
	/////////////////////

	float delta = 0.0, epsilon = 0.0;
	//Generate the node coordinates in the graph
	//This function also determines delta and epsilon,
	//where 'epsilon' is the connection radius and 'delta' is the 
	//neighborhood radius used during the Wasserstein-distance calculation
	if (!generateNodes(network, delta, epsilon))
		return false;

	//////////////////
	// NODE LINKING //
	//////////////////

	//Given the coordinates, link the nodes
	//When possible, this can be parallelized across one or more GPUs
	#ifdef CUDA_ENABLED
	if (network->props.use_gpu && network->props.N >= GPU_MIN) {
		//This is the GPU version of the node-linking algorithm
		if (!linkNodesGPU_v2(network, res, cu, epsilon))
			return false;
	} else
	#endif
	{
		//This is the non-GPU version
		if (!linkNodes(network, cu, epsilon))
			return false;
	}

	printf("Network construction completed.\n"); fflush(stdout);

	//////////////////////
	// PRINT GRAPH INFO //
	//////////////////////

	unsigned X = 0, Y = 1;
	//Assert that the two target nodes are linked in the classic setting (delta == epsilon)
	if ((network->props.classic && network->props.sep == SEP_DELTA) || (!network->props.classic && network->props.sep == SEP_EPSILON))
		assert (network->adj[X].read(Y) && network->adj[Y].read(X));
	assert (X != Y);

	//Print statistics about the graph that has been generated
	printf_cyan();
	printf("\nNodes: %u\n", network->props.N);
	network->obs.k_max = *std::max_element(network->nodes.k, network->nodes.k + network->props.N);
	for (unsigned i = 0; i < network->props.N; i++)
		network->obs.k_avg += network->adj[i].count_bits();
	network->obs.k_avg /= network->props.N;
	printf("Average Degree: %.6f\n", network->obs.k_avg);
	float exp_k = expected_avg_degree(network->props.region, network->props.N, epsilon);
	if (exp_k == exp_k)
		printf(" > Expected value: %.6f\n", exp_k);
	printf("Maximum Degree: %u\n", network->obs.k_max);
	if (delta) {
		printf_dbg("Delta: %.6f\n", delta);
		printf_cyan();
		printf(" > Alpha: %.6f\n", network->props.alpha);
		printf(" > Chi:   %.6f\n", network->props.chi);
	}
	if (!network->props.classic && epsilon) {
		printf_dbg("Epsilon: %.6f\n", epsilon);
		printf_cyan();
		printf(" > Beta: %.6f\n", network->props.beta);
		printf(" > Psi:  %.6f\n", network->props.psi);
	}
	if (network->props.sep != SEP_ZERO) {
		printf("X = [%u]\tY = [%u]\n", X, Y);
		printf("Neighborhood Separation, d(X,Y): %.6f\n", distance(network->nodes.crd, network->props.region, X, Y, network->props.use_gpu));
	} else
		Y = 0;
	printf("Seed: %ld\n", network->props.seed);
	printf_std();
	fflush(stdout);

	////////////////////////
	// MEASURE COMPONENTS //
	// AND STRETCH        //
	////////////////////////

	//Measure the component statistics
	//This is a check to ensure there is one single component
	//i.e. all nodes are reachable from all other nodes
	measureConnectedComponents(network->nodes, network->adj, network->props.N, res);

	//Calculate the stretch, defined as the ratio of the weighted graph distance to the
	//underlying manifold (geodesic) distance. It is a measure of how close geodesics in the
	//graph approximate continuum geodesics
	if (network->props.calc_stretch)
		measureStretch_v2(network, res, epsilon, (uint64_t)network->props.N);

	////////////////////
	// IDENTIFY DELTA //
	// NEIGHBORHOODS  //
	////////////////////

	std::vector<unsigned> Xnbr, Ynbr;
	if (network->props.sep == SEP_ZERO) {
		//This compares the ball construction using the
		//weighted graph distance to that using the exact distances
		//It is mainly used for debugging...
		printf("\nIdentifying neighborhoods around the origin.\n");
		get_ball(network, res, Xnbr, delta, epsilon, X, true);
		get_ball_exact(network, Xnbr, delta, X);
	} else if (network->props.nh == NH_NEAREST) {
		//The neighborhoods are simply the nearest neighbors in the graph
		printf("\nIdentifying neighborhoods using nearest neighbors.\n");
		for (unsigned i = 0; i < network->props.N; i++) {
			if (network->adj[X].read(i))
				Xnbr.push_back(i);
			if (network->adj[Y].read(i))
				Ynbr.push_back(i);
		}
	} else if (network->props.nh == NH_DELTA) {
		//The neighborhood is a ball of radius delta, using weighted distances
		printf("\nIdentifying neighborhoods using delta (weighted).\n");
		get_ball(network, res, Xnbr, delta, epsilon, X, true);
		get_ball(network, res, Ynbr, delta, epsilon, Y, true);
	} else if (network->props.nh == NH_DELTA_DISCRETE) {
		//The neighborhood is a ball of radius delta, using unweighted (graph-hop) distances
		printf("\nIdentifying neighborhoods using delta (unweighted).\n");
		get_ball(network, res, Xnbr, delta, epsilon, X, false);
		get_ball(network, res, Ynbr, delta, epsilon, Y, false);
	} else if (network->props.nh == NH_EXACT) {
		//Use the exact neighborhoods without reference to edge lengths
		printf("\nIdentifying neighborhoods using exact radius.\n");
		get_ball_exact(network, Xnbr, delta, X);
		get_ball_exact(network, Ynbr, delta, Y);
	}
	printf("X has [%u] neighbors.\n", network->nodes.k[X]);
	printf("Y has [%u] neighbors.\n", network->nodes.k[Y]);
	if (network->props.nh != NH_NEAREST) {
		printf("Xnbr has [%u] entries [%u%%].\n", (unsigned)Xnbr.size(), 100 * (unsigned)Xnbr.size() / network->props.N);
		printf("Ynbr has [%u] entries [%u%%].\n", (unsigned)Ynbr.size(), 100 * (unsigned)Ynbr.size() / network->props.N);
	}

	/////////////////////
	// VISUALIZE GRAPH //
	/////////////////////

	//This is mainly used for debugging
	if (network->props.visualize) {
		std::ofstream osX("Xnbr.dat");
		for (size_t i = 0; i < Xnbr.size(); i++)
			osX << Xnbr[i] << "\n";
		osX.flush();
		osX.close();
		std::ofstream osY("Ynbr.dat");
		for (size_t i = 0; i < Ynbr.size(); i++)
			osY << Ynbr[i] << "\n";
		osY.flush();
		osY.close();
	}

	/////////////////////
	// GENERATE GRAPH  //
	// DISTANCE MATRIX //
	/////////////////////

	uint64_t npairsXY = Xnbr.size() * Ynbr.size();
	float dist_xy = -1.0;
	if (!network->props.test) {
		network->obs.distances = MALLOC<double, true>(npairsXY, res);

		memoryCheckpoint(res);
		printMemUsed("for Distance Matrix", res->hostMemUsed, res->devMemUsed, 0);

		//This step will take the most time comparatively
		//We calculate the shorest paths in the graph between all sources in Xnbr
		//and all targets in Ynbr. Hence there are Xnbr*Ynbr pairs in total.
		//This populates network->obs.distances, which are the shortest paths in the weighted
		//or unweighted graph.
		if (!measureDistances(network, res, cu, delta, epsilon, Xnbr, Ynbr, X, Y, dist_xy))
			return false;
		//dist_xy is the measured distance between the target nodes
		assert (dist_xy >= 0.0);
	}

	FREE(network->obs.measures, npairs, res);

	//If we're using the GA* algorithm, free these sparse data structures
	#ifdef CUDA_ENABLED
	if (network->props.sm == GA_STAR) {
		if (MAX_GPUS > 1 && network->props.use_gpu)
			cuCtxPushCurrent(cu->cuContext[0]);

		VFREE(network->h_links);
		VFREE(network->h_linkidx);
		VFREE(network->obs.h_measures);

		if (MAX_GPUS > 1 && network->props.use_gpu)
			cuCtxPopCurrent(&cu->cuContext[0]);
	}
	#endif

	res->hostMemUsed -= sizeof(BlockType) * network->adj[0].getNumBlocks() * network->props.N;
	VFREE(network->adj);

	////////////////////////////
	// CALCULATE WASSERSTEIN  //
	// DISTANCE AND CURVATURE //
	////////////////////////////

	if (!network->props.test) {
		float mean_distance = 0.0;
		for (uint64_t i = 0; i < npairsXY; i++)
			mean_distance += network->obs.distances[i];
		mean_distance /= npairsXY;
		printf("Mean graph distance: %.6f\n", mean_distance);

		//Calculate the Wasserstein distance using the distance matrix (see function below this one)
		network->obs.wasserstein = wasserstein(network->obs.distances, Xnbr, Ynbr, X, Y, res);
		//Calculate the Ollivier and Ricci curvatures
		//Note the normalization by dist_xy and not delta
		//This makes a difference in certain settings so make sure it's what you want
		if (delta && network->props.sep != SEP_ZERO) {
			network->obs.ollivier = 1.0 - (network->obs.wasserstein / dist_xy);
			network->obs.ricci = 2.0 * (2.0 * dim) * network->obs.ollivier / POW2(dist_xy);
			printf_cyan();
			printf("Ollivier Curvature: %.5f\n", network->obs.ollivier);
			printf("Ricci Curvature: %.5f\n\n", network->obs.ricci);
			printf_std();
			fflush(stdout);
		}

		//Save all results to HDF5 file format 
		save_to_h5(network);
		FREE(network->obs.distances, npairsXY, res);
	}

	VFREE(Xnbr);
	VFREE(Ynbr);

	return true;
}

//Calculate the Wasserstein distance using the MOSEK package
//Tested using MOSEK 9.0.103
double wasserstein(const double * const distances, std::vector<unsigned> &Xnbr, std::vector<unsigned> &Ynbr, unsigned int X, unsigned int Y, Resources * const res)
{
	printf("\nCalculating Wasserstein distance between node pair [%u - %u].\n", X, Y);
	memoryCheckpoint(res);
	printMemUsed("for Wasserstein Distance", res->hostMemUsed, res->devMemUsed, 0);
	fflush(stdout);

	//Size of the linear program
	unsigned kx = Xnbr.size();
	unsigned ky = Ynbr.size();

	int numvar = kx * ky;
	int numcon = kx + ky;
	int nnz = numvar << 1;
	printf("LP Variables:   %d\n", numvar);
	printf("LP Constraints: %d\n", numcon);
	fflush(stdout);

	//Linear constraint matrix 'a' (CSR format)
	double *aval = MALLOC<double>(nnz, res);
	std::fill(aval, aval + (nnz >> 1), 1.0);
	std::fill(aval + (nnz >> 1), aval + nnz, 1.0 / kx);

	int *asub = MALLOC<int>(nnz, res);
	std::iota(asub, asub + (nnz >> 1), 0);
	for (unsigned j = 0; j < ky; j++)
		for (unsigned i = 0; i < kx; i++)
			asub[(nnz>>1)+(j*kx)+i] = i * ky + j;

	int *aptrb = MALLOC<int>(numcon, res);
	for (unsigned i = 0; i < kx; i++)
		aptrb[i] = i * ky;
	for (unsigned j = 0; j < ky; j++)
		aptrb[j+kx] = numvar + j * kx;

	int *aptre = MALLOC<int>(numcon, res);
	for (int i = 0; i < numcon - 1; i++)
		aptre[i] = aptrb[i+1];
	aptre[numcon-1] = nnz;

	//Objective coefficients
	double *c = MALLOC<double>(numvar, res);
	for (int i = 0; i < numvar; i++)
		c[i] = distances[i] / kx;

	//Bounds on constraints
	double *b = MALLOC<double>(numcon, res);
	for (unsigned i = 0; i < kx; i++)
		b[i] = 1;
	for (unsigned j = 0; j < ky; j++)
		b[j+kx] = 1.0 / ky;

	//MOSEK environment handle
	MSKenv_t env = NULL;
	assert(MSK_makeenv(&env, NULL) == MSK_RES_OK);

	//MOSEK optimization task
	MSKtask_t task = NULL;
	assert(MSK_maketask(env, numcon, numvar, &task) == MSK_RES_OK);
	assert(MSK_linkfunctotaskstream(task, MSK_STREAM_LOG, NULL, printstr) == MSK_RES_OK);

	//Initialize constraints and variables
	assert(MSK_appendcons(task, numcon) == MSK_RES_OK);
	assert(MSK_appendvars(task, numvar) == MSK_RES_OK);

	for (int j = 0; j < numvar; j++) {
		//Load the objective
		assert(MSK_putcj(task, j, c[j]) == MSK_RES_OK);

		//Load the variable bounds
		assert(MSK_putvarbound(task, j, MSK_BK_RA, 0.0, 1.0) == MSK_RES_OK);
	}

	for (int i = 0; i < numcon; i++) {
		//Load the constraint bounds
		assert(MSK_putconbound(task, i, MSK_BK_FX, b[i], b[i]) == MSK_RES_OK);

		//Load constraint 'i'
		assert(MSK_putarow(task, i, aptre[i] - aptrb[i], asub + aptrb[i], aval + aptrb[i]) == MSK_RES_OK);
	}
	
	//Set objective goal to minimize
	assert(MSK_putobjsense(task, MSK_OBJECTIVE_SENSE_MINIMIZE) == MSK_RES_OK);

	//These commands disable multithreading
	//Uncomment them if you need to debug
	//MSK_putintparam(task, MSK_IPAR_INTPNT_MULTI_THREAD, MSK_OFF);
	//MSK_putintparam(task, MSK_IPAR_INTPNT_BASIS, MSK_BI_NEVER);

	//Use one of these two methods:

	//Enforce Primal Simplex Method
	//MSK_putintparam(task, MSK_IPAR_OPTIMIZER, MSK_OPTIMIZER_PRIMAL_SIMPLEX);
	//Enforce Interior Point Method
	MSK_putintparam(task, MSK_IPAR_OPTIMIZER, MSK_OPTIMIZER_INTPNT);

	//Run the optimizer
	MSKrescodee trmcode;
	assert(MSK_optimizetrm(task, &trmcode) == MSK_RES_OK);

	//Print a summary
	MSK_solutionsummary(task, MSK_STREAM_LOG);
	MSKsolstae solsta;
	assert(MSK_getsolsta(task, MSK_SOL_BAS, &solsta) == MSK_RES_OK);
	assert (solsta == MSK_SOL_STA_OPTIMAL);

	//The optimal value (Wasserstein distance)
	double W = 0;
	MSK_getprimalobj(task, MSK_SOL_BAS, &W);
	printf_cyan();
	printf("\nWasserstein Distance: %.6f\n", W);
	printf_std();

	//Delete the task and environment
	MSK_deletetask(&task);
	MSK_deleteenv(&env);

	//Free variables
	FREE(aval, nnz, res);
	FREE(asub, nnz, res);
	FREE(aptrb, numcon, res);
	FREE(aptre, numcon, res);
	FREE(b, numcon, res);
	FREE(c, numvar, res);

	return W;
}

//Free the remainder of the allocated memory
void destroyNetwork(Network * const network, Resources * const res, CuResources * const cu)
{
	#ifdef CUDA_ENABLED
	if (MAX_GPUS > 1 && network->props.use_gpu)
		checkCudaErrors(cuCtxPushCurrent(cu->cuContext[0]));
	#endif

	unsigned int dim = (network->props.region == R1) ? 1 : 2;
	if (network->props.use_gpu) {
		FREE<float, true>(network->nodes.crd->h_points, dim * network->props.N, res);
		FREE<unsigned, true>(network->nodes.k, network->props.N, res);
	} else {
		for (unsigned int i = 0; i < dim; i++)
			FREE(network->nodes.crd->points[i], network->props.N, res);
		FREE<unsigned>(network->nodes.k, network->props.N, res);
	}

	#ifdef CUDA_ENABLED
	if (network->props.use_gpu)
		checkCudaErrors(cuCtxPopCurrent(cu->cuContext));
	#endif

	return;
}
