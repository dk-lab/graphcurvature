#include "subroutines.h"
#ifdef CUDA_ENABLED
#include "kernels/link_kernels.cuh"
#endif

/////////////////////////////
//(C) Will Cunningham 2018 //
//         DK Lab          //
// Northeastern University //
/////////////////////////////

//Generate node coordinates (Poisson sprinkling)
bool generateNodes(Network *network, float &delta, float &epsilon)
{
	//There are a few different prescriptions for defining delta and epsilon...
	//they are hard-coded in here but generally rely on the parameters 'alpha' and 'beta'

	//delta = network->props.alpha * sqrt(log((double)network->props.N)) * pow(network->props.N, -network->props.chi);
	delta = network->props.alpha * pow(network->props.N, -network->props.chi);
	printf_dbg("NOTE: Using modified delta: alpha*N^-chi\n");
	epsilon = network->props.beta * pow(network->props.N, -network->props.psi);
	if (network->props.classic)
		epsilon = delta;

	switch (network->props.region) {
	case BINARY_TREE:
	case Z2:
	case S1:
	case COMPLETE:
		//Coordinates not needed
		break;
	case R1:	//Real line in 1D
		delta = pow((float)network->props.N, -network->props.alpha);
		for (unsigned i = 0; i < network->props.N; i++)
			network->nodes.crd->points[0][i] = network->props.mrng.rng() - 0.5;
		break;
	case T2:	//Torus in 2D
		//We enforce a maximum radius to ensure neighborhoods do not wrap around onto themselves
		network->obs.rmax = sqrt(2.0) / 2.0;
		//Sprinkle N nodes into a unit square
		for (unsigned i = 0; i < network->props.N; i++) {
			if (network->props.use_gpu) {
				network->nodes.crd->h_points[i<<1] = network->props.mrng.rng();
				network->nodes.crd->h_points[(i<<1)+1] = network->props.mrng.rng();
			} else {
				network->nodes.crd->points[0][i] = network->props.mrng.rng();
				network->nodes.crd->points[1][i] = network->props.mrng.rng();
			}
		}

		//x* is placed at the center
		//y* is placed at distance 'delta' away on the diagonal
		if (network->props.sep != SEP_RANDOM) {
			if (network->props.use_gpu) {
				network->nodes.crd->h_points[0] = 0.5;
				network->nodes.crd->h_points[1] = 0.5;
				//The factor 0.999999 ensures the target nodes are connected geometrically
				//Without it sometimes numerical errors can arise which make them disconnected
				if (network->props.sep != SEP_ZERO) {
					network->nodes.crd->h_points[2] = 0.999999 * (network->props.sep == SEP_DELTA ? delta : epsilon) * cos(M_PI / 4.0) + 0.5;
					network->nodes.crd->h_points[3] = 0.999999 * (network->props.sep == SEP_DELTA ? delta : epsilon) * sin(M_PI / 4.0) + 0.5;
				}
			} else {
				network->nodes.crd->points[0][0] = 0.5;
				network->nodes.crd->points[1][0] = 0.5;
				if (network->props.sep != SEP_ZERO) {
					network->nodes.crd->points[0][1] = 0.999999 * (network->props.sep == SEP_DELTA ? delta : epsilon) * cos(M_PI / 4.0) + 0.5;
					network->nodes.crd->points[1][1] = 0.999999 * (network->props.sep == SEP_DELTA ? delta : epsilon) * sin(M_PI / 4.0) + 0.5;
				}
			}
		}
		break;
	case S2:	//Two-dimensional sphere
		network->obs.rmax = M_PI;
		//Sprinkle N nodes into a unit 2-sphere
		for (unsigned i = 0; i < network->props.N; i++) {
			if (network->props.use_gpu) {
				network->nodes.crd->h_points[i<<1] = acos(1.0 - 2.0 * network->props.mrng.rng());	//Zenith Angle
				network->nodes.crd->h_points[(i<<1)+1] = TWO_PI * network->props.mrng.rng(); //Azimuthal Angle
			} else {
				network->nodes.crd->points[0][i] = acos(1.0 - 2.0 * network->props.mrng.rng());	//Zenith Angle
				network->nodes.crd->points[1][i] = TWO_PI * network->props.mrng.rng(); //Azimuthal Angle
			}
		}

		//x* is placed at the equator
		//y* is placed at distance delta away, also on the equator
		if (network->props.sep != SEP_RANDOM) {
			if (network->props.use_gpu) {
				network->nodes.crd->h_points[0] = HALF_PI;
				network->nodes.crd->h_points[1] = 0.0;
				if (network->props.sep != SEP_ZERO) {
					network->nodes.crd->h_points[2] = HALF_PI;
					network->nodes.crd->h_points[3] = 0.9999999 * (network->props.sep == SEP_DELTA ? delta : epsilon);
				}
			} else {
				network->nodes.crd->points[0][0] = HALF_PI;
				network->nodes.crd->points[1][0] = 0.0;
				if (network->props.sep != SEP_ZERO) {
					network->nodes.crd->points[0][1] = HALF_PI;
					network->nodes.crd->points[1][1] = 0.9999999 * (network->props.sep == SEP_DELTA ? delta : epsilon);
				}
			}
		}
		break;
	case BOLZA:	//2D Bolza surface
	{
		float R = 2.0 * atanh(pow(2.0, -0.25));
		network->obs.rmax = R;
		//We use rejection sampling for the Bolza surface
		//This sprinkles points into a disk and then rejects points
		//between the fundamental octagon and its circumscription
		for (unsigned i = 0; i < network->props.N; i++) {
			//Angular coordinate
			float theta = TWO_PI * network->props.mrng.rng();
			//Radius in hyperbolic coordinates
			float rH = acosh(network->props.mrng.rng() * (cosh(R) - 1.0) + 1.0);
			//Radius in Poincare representation
			float rP = tanh(rH / 2.0);
			//Radius in Beltrami-Klein representation
			float rK = 2.0 * rP / (1.0 + POW2(rP));
			float k = (unsigned)floor(4.0 * (theta - M_PI / 8.0) / M_PI) & 7;
			float phi = theta - (1.0 + 2.0 * k) * M_PI / 8.0;
			phi -= TWO_PI * floor(phi / TWO_PI);	//This gives (phi mod 2pi)
			float rc = pow(2.0, 1.25) * cos(M_PI / 8.0) / (1.0 + sqrt(2.0));
			if (phi < M_PI / 8.0)
				rc /= cos(M_PI / 8.0 - phi);
			else
				rc /= cos(phi - M_PI / 8.0);
			if (rK > rc) {	//The point should be rejected
				i--;
				continue;
			}

			if (network->props.use_gpu) {
				network->nodes.crd->h_points[i<<1] = rP * cos(theta);
				network->nodes.crd->h_points[(i<<1)+1] = rP * sin(theta);
			} else {
				network->nodes.crd->points[0][i] = rP * cos(theta);
				network->nodes.crd->points[1][i] = rP * sin(theta);
			}
		}

		//x* is placed at the origin
		//y* is placed distance delta away on the pi/8 diagonal
		if (network->props.sep != SEP_RANDOM) {
			if (network->props.use_gpu) {
				network->nodes.crd->h_points[0] = 0.0;
				network->nodes.crd->h_points[1] = 0.0;
				if (network->props.sep != SEP_ZERO) {
					network->nodes.crd->h_points[2] = tanh(0.999999 * (network->props.sep == SEP_DELTA ? delta : epsilon) / 2) * cos(M_PI / 8.0);
					network->nodes.crd->h_points[3] = tanh(0.999999 * (network->props.sep == SEP_DELTA ? delta : epsilon) / 2) * sin(M_PI / 8.0);
				}
			} else {
				network->nodes.crd->points[0][0] = 0.0;
				network->nodes.crd->points[1][0] = 0.0;
				if (network->props.sep != SEP_ZERO) {
					network->nodes.crd->points[0][1] = tanh(0.9999 * (network->props.sep == SEP_DELTA ? delta : epsilon) / 2) * cos(M_PI / 8.0);
					network->nodes.crd->points[1][1] = tanh(0.9999 * (network->props.sep == SEP_DELTA ? delta : epsilon) / 2) * sin(M_PI / 8.0);
				}
			}
		}
		break;
	}
	default:
		printf("Not yet implemented!\n");
		return false;
	}

	if (delta) {
		//Make sure delta is not larger than the maximum radius
		if (delta > network->obs.rmax) {
			printf("Delta [%f] is larger than rmax [%f]. Decrease alpha.\n", delta, network->obs.rmax);
			return false;
		}
		//Make sure delta is greater than epsilon
		if (delta < epsilon) {
			printf("Delta [%f] is smaller than epsilon [%f]. Increase alpha or decrease beta.\n", delta, epsilon);
			return false;
		}
	}

	//Visualize nodes
	if (network->props.visualize) {
		std::ofstream os("nodes.dat");
		for (unsigned int i = 0; i < network->props.N; i++) {
			if (network->props.use_gpu)
				os << network->nodes.crd->h_points[i<<1] << " " << network->nodes.crd->h_points[(i<<1)+1] << "\n";
			else
				os << network->nodes.crd->points[0][i] << " " << network->nodes.crd->points[1][i] << "\n";
		}
		os.flush();
		os.close();
	}

	return true;
}

//Link node pairs separated by distance epsilon or less
bool linkNodes(Network *network, CuResources * const cu, const float delta)
{
	unsigned nlayers;
	unsigned width;
	unsigned n = network->props.N + (network->props.N & 1);
	uint64_t npairs = (uint64_t)n * (n - 1) / 2;
	assert (network->props.region == R1 || network->props.region == BINARY_TREE || network->props.region == Z2 || network->props.region == S1 || network->props.region == COMPLETE || network->props.region == T2 || network->props.region == S2 || network->props.region == BOLZA);
	assert (delta > 0.0);

	#ifdef CUDA_ENABLED
	if (network->props.use_gpu && MAX_GPUS > 1)
		cuCtxPushCurrent(cu->cuContext[0]);
	#endif

	Stopwatch slink = Stopwatch();
	stopwatchStart(&slink);

	printf("Linking Nodes (CPU)\n"); fflush(stdout);

	switch (network->props.region) {
	case T2:
	case S2:
	case R1:
	case BOLZA:
	{
		std::vector<unsigned> links0;
		links0.reserve((uint64_t)network->props.N * (network->props.N - 1) / 20);
		printf_dbg(" > Executing algorithm with [%d] threads\n", network->props.N >= 256 ? omp_get_max_threads() : 1);
		fflush(stdout);
		#ifdef _OPENMP
		#pragma omp parallel for schedule (static, 16) if (network->props.N >= 256)
		#endif
		for (uint64_t k = 0; k < npairs; k++) {
			unsigned int i = (int)(k / (n - 1));
			unsigned int j = (int)(k % (n - 1) + 1);
			int do_map = (i >= j);
			i += do_map * ((((n >> 1) - i) << 1) - 1);
			j += do_map * (((n >> 1) - j) << 1);
			if (j == network->props.N) continue;

			float d = distance(network->nodes.crd, network->props.region, i, j, network->props.use_gpu);
			assert (d == d);

			if (d <= delta) {
				#ifdef _OPENMP
				#pragma omp critical
				#endif
				{
					network->adj[i].set(j);
					network->adj[j].set(i);
				}

				#ifdef _OPENMP
				#pragma omp atomic
				#endif
				network->nodes.k[i]++;

				#ifdef _OPENMP
				#pragma omp atomic
				#endif
				network->nodes.k[j]++;

				if (network->props.use_gpu && network->props.sm == GA_STAR) {
					#ifdef _OPENMP
					#pragma omp critical
					#endif
					{
						links0.push_back(i);
						links0.push_back(j);
						network->h_links.push_back(j);
						network->h_links.push_back(i);
						network->obs.h_measures.push_back(d);
						network->obs.h_measures.push_back(d);
					}
				}
			}

			//Record edge weights in 'measures'
			network->obs.measures[index_map(i, j, network->props.N)] = d;
		}

		//If using GA*, we also store a sparse representation
		//which must be sorted (CSR format)
		if (network->props.use_gpu && network->props.sm == GA_STAR) {
			quicksort(links0, network->h_links, network->obs.h_measures, 0, links0.size() - 1);

			uint64_t idx = 0;
			for (unsigned i = 0; i < network->props.N; i++) {
				network->h_linkidx[i] = idx;
				idx += network->nodes.k[i];
			}
		}
		break;
	}
	case BINARY_TREE:
		nlayers = (unsigned)log2(network->props.N + 1.0) - 1;
		for (unsigned layer = 0; layer < nlayers; layer++) {
			unsigned offset = (1 << layer) - 1;
			unsigned nodes_in_layer = 1 << layer;
			for (unsigned i = 0; i < nodes_in_layer; i++) {
				network->adj[offset+i].set(offset+nodes_in_layer+2*i);
				network->adj[offset+nodes_in_layer+2*i].set(offset+i);
				network->adj[offset+i].set(offset+nodes_in_layer+2*i+1);
				network->adj[offset+nodes_in_layer+2*i+1].set(offset+i);
			}
		}

		network->nodes.k[0] = 2;
		for (unsigned i = 1; i < ((network->props.N + 1) >> 1) - 1; i++)
			network->nodes.k[i] = 3;
		for (unsigned i = ((network->props.N + 1) >> 1) - 1; i < network->props.N; i++)
			network->nodes.k[i] = 1;
		break;
	case Z2:	//2D lattice (grid)
		width = sqrt(network->props.N);
		for (unsigned i = 0; i < network->props.N; i++) {
			//Link left
			if (i % width)
				network->adj[i].set(i-1);
			//Link right
			if ((i + 1) % width)
				network->adj[i].set(i+1);
			//Link up
			if (i >= width)
				network->adj[i].set(i - width);
			//Link down
			if (i < network->props.N - width)
				network->adj[i].set(i + width);
		}

		//Four corners
		network->nodes.k[0] = 2;
		network->nodes.k[width-1] = 2;
		network->nodes.k[network->props.N-width] = 2;
		network->nodes.k[network->props.N-1] = 2;
		//Sides
		for (unsigned i = 0; i < width - 2; i++) {
			network->nodes.k[i+1] = 3;
			network->nodes.k[(i+1)*width] = 3;
			network->nodes.k[(i+2)*width-1] = 3;
			network->nodes.k[network->props.N-width+i+1] = 3;
		}
		//Interior
		for (unsigned i = 1; i < width - 1; i++)
			for (unsigned j = 1; j < width - 1; j++)
				network->nodes.k[i*width+j] = 4;
		break;
	case S1:	//1D circle
		for (unsigned i = 0; i < network->props.N - 1; i++) {
			network->adj[i].set(i+1);
			network->adj[i+1].set(i);
		}
		network->adj[0].set(network->props.N-1);
		network->adj[network->props.N-1].set(0);
		for (unsigned i = 0; i < network->props.N; i++)
			network->nodes.k[i] = 2;
		break;
	case COMPLETE:	//Complete graph
		for (unsigned i = 0; i < network->props.N; i++) {
			network->adj[i].flip();
			network->adj[i].unset(i);
			network->nodes.k[i] = network->props.N - 1;
		}
		break;
	default:
		printf("Not yet implemented!\n");
		return false;
	}

	stopwatchStop(&slink);
	printf("Elapsed Time: %.6f\n", slink.elapsedTime);
	fflush(stdout);

	#ifdef CUDA_ENABLED
	if (network->props.use_gpu && MAX_GPUS > 1)
		cuCtxPopCurrent(cu->cuContext);
	#endif

	return true;
}

//Link nodes using the GPU instead
//When multiple GPUs are available or when the adjacency matrix
//is too large to fit in GPU memory, we break it up into tiles,
//constructed one at a time
#ifdef CUDA_ENABLED
bool linkNodesGPU_v2(Network *network, Resources * const res, CuResources * const cu, const float delta)
{
	Stopwatch sgpu = Stopwatch();
	stopwatchStart(&sgpu);

	printf("Linking Nodes (GPU) [VER 3]\n"); fflush(stdout);
	dim3 threads_per_block(1, GPU_BLOCK_SIZE, 1);

	//First check how much memory is expected to be used
	//This will guide how we tile the problem
	long mem = GLOB_MEM + 1L;
	float ng = 0.5f;
	long blocks_per_group_x = 0, threads_per_group_x = 0, threads_per_group = 0;
	while (mem > GLOB_MEM || (network->props.N >= GPU_MIN * MAX_GPUS && ng < MAX_GPUS)) {
		ng *= 2.0f;
		blocks_per_group_x = (long)ceil((float)network->props.N / (GPU_BLOCK_SIZE * ng));
		threads_per_group_x = blocks_per_group_x * GPU_BLOCK_SIZE;
		threads_per_group = POW2(threads_per_group_x);
	
		mem = 20 * threads_per_group_x;	//Coordinates (4x 4 bytes), Degrees (4 bytes)
		mem += 5 * threads_per_group;	//Edges (1 byte), Measures (4 bytes)
		if (network->props.region == BOLZA)
			mem += 768;		//Generators (4x48 x 4 bytes)
	}

	printf("nodes: %u\n", network->props.N);
	unsigned ngroups = (unsigned)ng;
	printf("ngroups: %u\n", ngroups);
	cu->gpus_active = std::min(ngroups, (unsigned)MAX_GPUS);
	printf("using %u gpu(s)\n", cu->gpus_active);
	unsigned groups_per_gpu = ngroups / cu->gpus_active;

	//CUDA grid (per GPU)
	unsigned blocks_per_grid_x, blocks_per_grid_y;
	if (network->props.region == BOLZA) {
		blocks_per_grid_x = threads_per_group_x;
		blocks_per_grid_y = blocks_per_group_x << 5;
	} else {
		blocks_per_grid_x = (unsigned)ceil((float)threads_per_group_x / LINKS_PER_THREAD);
		blocks_per_grid_y = blocks_per_group_x;
	}
	printf("Grid: [%u, %u]\n", blocks_per_grid_x, blocks_per_grid_y);
	dim3 blocks_per_grid(blocks_per_grid_x, blocks_per_grid_y, 1);

	if (MAX_GPUS > 1 && cu->gpus_active == 1)
		cuCtxPushCurrent(cu->cuContext[0]);

	//Variables passed to GPU
	CUdeviceptr d_z0[MAX_GPUS], d_z1[MAX_GPUS];
	CUdeviceptr d_generators[MAX_GPUS];
	CUdeviceptr d_k[MAX_GPUS];
	CUdeviceptr d_edges[MAX_GPUS];
	CUdeviceptr d_measures[MAX_GPUS];
	unsigned *h_k[MAX_GPUS];
	uint8_t *h_edges[MAX_GPUS];
	float *h_measures[MAX_GPUS];

	//Allocate GPU memory
	for (unsigned i = 0; i < cu->gpus_active; i++) {
		if (cu->gpus_active > 1)
			cuCtxPushCurrent(cu->cuContext[i]);

		//Node coordinates
		d_z0[i] = CUDA_MALLOC<cuComplex>(threads_per_group_x, res);
		d_z1[i] = CUDA_MALLOC<cuComplex>(threads_per_group_x, res);
		//Degrees
		d_k[i] = CUDA_MALLOC<unsigned>(threads_per_group_x, res);
		//Bolza generators
		if (network->props.region == BOLZA)
			d_generators[i] = CUDA_MALLOC<float4>(48, res, (float4*)generators);
		//Sparse edge list
		d_edges[i] = CUDA_MALLOC<uint8_t>(threads_per_group, res);
		//Measures (edge weights)
		d_measures[i] = CUDA_MALLOC<float>(threads_per_group, res);

		//Buffers used to copy the above back to RAM
		h_k[i] = MALLOC<unsigned, false, true>(threads_per_group_x, res);
		h_edges[i] = MALLOC<uint8_t, false, true>(threads_per_group, res);
		h_measures[i] = MALLOC<float, false, true>(threads_per_group, res);

		if (cu->gpus_active > 1)
			cuCtxPopCurrent(&cu->cuContext[i]);
	}

	memoryCheckpoint(res);
	printMemUsed("for Generating Lists on GPU", res->hostMemUsed, res->devMemUsed, 0);

	size_t final_size = network->props.N - threads_per_group_x * (ngroups - 1);
	size_t size0, size1, size0_old, size1_old;

	std::vector<unsigned> links0;
	links0.reserve((uint64_t)network->props.N * (network->props.N - 1) / 20);

	long total_threads = (long)blocks_per_grid.x * blocks_per_grid.y * threads_per_block.y;
	printf_dbg(" > Executing algorithm with [%lu] threads across [%u] GPU(s)\n", total_threads, cu->gpus_active);
	printf_dbg("   Block ID    GPU\n");
	printf_dbg("   ---------------\n");
	fflush(stdout);

	//Run the first block on each GPU
	size1 = (ngroups == 1) ? final_size : threads_per_group_x;
	for (unsigned k = 0; k < cu->gpus_active; k++) {
		size0 = (groups_per_gpu == 1 && k == cu->gpus_active - 1) ? final_size : threads_per_group_x;
		if (cu->gpus_active > 1)
			cuCtxPushCurrent(cu->cuContext[k]);
		printf_dbg("   Tile (%u, 0)      [%u]\n", k * groups_per_gpu, k); fflush(stdout);

		//Initialize Input Buffers
		checkCudaErrors(cuMemsetD32Async(d_k[k], 0, threads_per_group_x, 0));
		checkCudaErrors(cuMemsetD8Async(d_edges[k], 0, threads_per_group, 0));
		checkCudaErrors(cuMemcpyHtoDAsync(d_z0[k], network->nodes.crd->h_points + 2 * (k * groups_per_gpu) * threads_per_group_x, sizeof(cuComplex) * size0, 0));
		checkCudaErrors(cuMemcpyHtoDAsync(d_z1[k], network->nodes.crd->h_points, sizeof(cuComplex) * size1, 0));

		//Execute Kernel
		if (network->props.region == T2)
			link<T2,GPU_BLOCK_SIZE,LINKS_PER_THREAD><<<blocks_per_grid, threads_per_block>>>((float2*)d_z0[k], (float2*)d_z1[k], (unsigned*)d_k[k], (uint8_t*)d_edges[k], (float*)d_measures[k], size0, size1, delta);
		else if (network->props.region == S2)
			link<S2,GPU_BLOCK_SIZE,LINKS_PER_THREAD><<<blocks_per_grid, threads_per_block>>>((float2*)d_z0[k], (float2*)d_z1[k], (unsigned*)d_k[k], (uint8_t*)d_edges[k], (float*)d_measures[k], size0, size1, delta);
		else if (network->props.region == BOLZA)
			bolza_v1<<<blocks_per_grid, threads_per_block>>>((cuComplex*)d_z0[k], (cuComplex*)d_z1[k], (unsigned*)d_k[k], (unsigned*)d_edges[k], (float*)d_measures[k], (float4*)d_generators[k], size0, size1, delta);

		if (cu->gpus_active > 1)
			cuCtxPopCurrent(&cu->cuContext[k]);
	}

	unsigned i_old = 0, j_old = 0;
	for (unsigned i = 0; i < groups_per_gpu; i++) {
		for (unsigned j = 0; j < ngroups; j++) {	//m = i * ngroups + j ; i = m / ngroups ; j = m % ngroups
			if (!i && !j) continue;	//We already did the first tile
			i_old = (i * ngroups + j - 1) / ngroups;
			j_old = (i * ngroups + j - 1) % ngroups;
			size1 = (j == ngroups - 1) ? final_size : threads_per_group_x;
			size1_old = (j_old == ngroups - 1) ? final_size : threads_per_group_x;

			//Copy device buffers to the host
			for (unsigned k = 0; k < cu->gpus_active; k++) {
				if (cu->gpus_active > 1)
					cuCtxPushCurrent(cu->cuContext[k]);

				checkCudaErrors(cuMemcpyDtoHAsync(h_k[k], d_k[k], sizeof(unsigned) * threads_per_group_x, 0));
				checkCudaErrors(cuMemcpyDtoHAsync(h_edges[k], d_edges[k], sizeof(uint8_t) * threads_per_group, 0));
				checkCudaErrors(cuMemcpyDtoHAsync(h_measures[k], d_measures[k], sizeof(float) * threads_per_group, 0));

				if (cu->gpus_active > 1)
					cuCtxPopCurrent(&cu->cuContext[k]);
			}
			
			//Synchronize
			sync_all_gpus(cu);

			//Calculate the new values
			for (unsigned k = 0; k < cu->gpus_active; k++) {
				if (cu->gpus_active > 1)
					cuCtxPushCurrent(cu->cuContext[k]);
				
				size0 = (i == groups_per_gpu - 1 && k == cu->gpus_active - 1) ? final_size : threads_per_group_x;
				printf_dbg("   (%u, %u)      [%u]\n", k * groups_per_gpu + i, j, k); fflush(stdout);

				//Zero the buffers
				checkCudaErrors(cuMemsetD32Async(d_k[k], 0, threads_per_group_x, 0));
				checkCudaErrors(cuMemsetD8Async(d_edges[k], 0, threads_per_group, 0));
				//Copy coordinates to device
				checkCudaErrors(cuMemcpyHtoD(d_z0[k], network->nodes.crd->h_points + 2 * (k * groups_per_gpu + i) * threads_per_group_x, sizeof(cuComplex) * size0));
				checkCudaErrors(cuMemcpyHtoD(d_z1[k], network->nodes.crd->h_points + 2 * j * threads_per_group_x, sizeof(cuComplex) * size1));

				//Execute the kernel
				if (network->props.region == T2)
					link<T2,GPU_BLOCK_SIZE,LINKS_PER_THREAD><<<blocks_per_grid, threads_per_block>>>((float2*)d_z0[k], (float2*)d_z1[k], (unsigned*)d_k[k], (uint8_t*)d_edges[k], (float*)d_measures[k], size0, size1, delta);
				else if (network->props.region == S2)
					link<S2,GPU_BLOCK_SIZE,LINKS_PER_THREAD><<<blocks_per_grid, threads_per_block>>>((float2*)d_z0[k], (float2*)d_z1[k], (unsigned*)d_k[k], (uint8_t*)d_edges[k], (float*)d_measures[k], size0, size1, delta);
				else if (network->props.region == BOLZA)
					bolza_v1<<<blocks_per_grid, threads_per_block>>>((cuComplex*)d_z0[k], (cuComplex*)d_z1[k], (unsigned*)d_k[k], (unsigned*)d_edges[k], (float*)d_measures[k], (float4*)d_generators[k], size0, size1, delta);

				if (cu->gpus_active > 1)
					cuCtxPopCurrent(&cu->cuContext[k]);
			}
			
			for (unsigned k = 0; k < cu->gpus_active; k++) {
				if (cu->gpus_active > 1)
					cuCtxPushCurrent(cu->cuContext[k]);
				size0_old = (i_old == groups_per_gpu - 1 && k == cu->gpus_active - 1) ? final_size : threads_per_group_x;
				//Record the node degrees
				readDegrees(network->nodes.k, h_k[k], j_old * threads_per_group_x, size1_old);
				//Record the adjacency submatrix and the measures
				if (network->props.sm == GA_STAR)
					readData(network->adj, links0, network->h_links, h_edges[k], network->obs.measures, network->obs.h_measures, h_measures[k], network->props.N, k * groups_per_gpu + i_old, j_old, threads_per_group_x, size0_old, size1_old, true);
				else {
					readEdges(network->adj, h_edges[k], k * groups_per_gpu + i_old, j_old, threads_per_group_x, size0_old, size1_old);
					readMeasures(network->obs.measures, h_measures[k], network->props.N, k * groups_per_gpu + i_old, j_old, threads_per_group_x, size0_old, size1_old);
				}
				if (cu->gpus_active > 1)
					cuCtxPopCurrent(&cu->cuContext[k]);
			}
		}
	}

	//Record the final values
	for (unsigned k = 0; k < cu->gpus_active; k++) {
		if (cu->gpus_active > 1)
			cuCtxPushCurrent(cu->cuContext[k]);

		size0 = (k == cu->gpus_active - 1) ? final_size : threads_per_group_x;
		size1 = final_size;

		checkCudaErrors(cuMemcpyDtoHAsync(h_k[k], d_k[k], sizeof(unsigned) * threads_per_group_x, 0));
		checkCudaErrors(cuMemcpyDtoHAsync(h_edges[k], d_edges[k], sizeof(uint8_t) * threads_per_group, 0));
		checkCudaErrors(cuMemcpyDtoHAsync(h_measures[k], d_measures[k], sizeof(float) * threads_per_group, 0));
		
		checkCudaErrors(cuCtxSynchronize());

		readDegrees(network->nodes.k, h_k[k], (ngroups - 1) * threads_per_group_x, size1);
		if (network->props.sm == GA_STAR)
			readData(network->adj, links0, network->h_links, h_edges[k], network->obs.measures, network->obs.h_measures, h_measures[k], network->props.N, (k + 1) * groups_per_gpu - 1, ngroups - 1, threads_per_group_x, size0, size1, true);			
		else {
			readEdges(network->adj, h_edges[k], (k + 1) * groups_per_gpu - 1, ngroups - 1, threads_per_group_x, size0, size1);
			readMeasures(network->obs.measures, h_measures[k], network->props.N, (k + 1) * groups_per_gpu - 1, ngroups - 1, threads_per_group_x, size0, size1);
		}

		if (cu->gpus_active > 1)
			cuCtxPopCurrent(&cu->cuContext[k]);
	}

	//This is needed for GA*, since the distance function can be asymmetric due to numerical errors
	for (unsigned i = 0; i < network->props.N; i++)
		network->nodes.k[i] = (unsigned)network->adj[i].count_bits();

	//Format the sparse data structures
	if (network->props.sm == GA_STAR) {
		printf("\nFormatting data structures.\n");
		//Sort the lists
		quicksort(links0, network->h_links, network->obs.h_measures, 0, links0.size() - 1);

		uint64_t idx = 0;
		for (unsigned i = 0; i < network->props.N; i++) {
			network->h_linkidx[i] = idx;
			idx += network->nodes.k[i];
		}
	}

	//Free GPU memory
	for (unsigned i = 0; i < cu->gpus_active; i++) {
		if (cu->gpus_active > 1)
			cuCtxPushCurrent(cu->cuContext[i]);

		CUDA_FREE<cuComplex>(d_z0[i], threads_per_group_x, res);
		CUDA_FREE<cuComplex>(d_z1[i], threads_per_group_x, res);
		CUDA_FREE<unsigned>(d_k[i], threads_per_group_x, res);
		if (network->props.region == BOLZA)
			CUDA_FREE<float4>(d_generators[i], 48, res);

		CUDA_FREE<uint8_t>(d_edges[i], threads_per_group, res);
		CUDA_FREE<float>(d_measures[i], threads_per_group, res);

		FREE<unsigned, true>(h_k[i], threads_per_group_x, res);
		FREE<uint8_t, true>(h_edges[i], threads_per_group, res);
		FREE<float, true>(h_measures[i], threads_per_group, res);

		if (cu->gpus_active > 1)
			cuCtxPopCurrent(&cu->cuContext[i]);
	}

	//Synchronize
	sync_all_gpus(cu);

	if (MAX_GPUS > 1 && cu->gpus_active == 1)
		cuCtxPopCurrent(cu->cuContext);

	stopwatchStop(&sgpu);
	printf("Elapsed Time: %.6f sec\n", sgpu.elapsedTime);
	fflush(stdout);

	return true;
}

#endif

//Calculate the mean stretch
void measureStretch_v2(Network *network, Resources *res, const float epsilon, const uint64_t npairs)
{
	printf("\nPreparing to measure stretch...\n");
	fflush(stdout);

	//Calculate the stretch for 'npairs' samples and then take the mean
	double *samples = MALLOC<double, true>(npairs, res);

	//Workspaces
	float *dwork = MALLOC<float, true>(network->props.N * omp_get_max_threads(), res);
	Bitvector awork;
	FastBitset fb(network->props.N);
	for (int i = 0; i < omp_get_max_threads(); i++)
		awork.push_back(fb);
	res->hostMemUsed += sizeof(BlockType) * awork[0].getNumBlocks() * omp_get_max_threads();
	PriorityQueue<unsigned, float> frontier[omp_get_max_threads()];
	std::vector<unsigned> next[omp_get_max_threads()];

	memoryCheckpoint(res);
	printMemUsed("to Measure Stretch", res->hostMemUsed, res->devMemUsed, 0);

	Stopwatch stp = Stopwatch();
	stopwatchStart(&stp);

	printf_dbg(" > Executing algorithm with [%d] threads\n\n", npairs >= 1024 ? omp_get_max_threads() : 1);
	fflush(stdout);

	//Use a parallel RNG if using multiple cores
	#ifdef _OPENMP
	unsigned seed = (unsigned)(network->props.mrng.rng() * UINT_MAX);
	#pragma omp parallel if (npairs >= 1024)
	{
		Engine eng(seed ^ omp_get_thread_num());
		UDistribution dst(0.0, 1.0);
		UGenerator rng(eng, dst);
		#pragma omp for schedule (dynamic, 16)
	#else
		//If not parallel, just use a reference to the main RNG
		UGenerator &rng = network->props.mrng.rng;
	#endif
		for (uint64_t p = 0; p < npairs; p++) {
			unsigned tid = omp_get_thread_num();
			unsigned i = rng() * network->props.N;
			unsigned j = rng() * (network->props.N - 1);
			if (i == j) j++;
			
			//Unweighted shortest path (graph distance)
			double graph_distance;
			if (!network->props.weighted)
				graph_distance = shortestPathUnweighted(network->adj, network->nodes.k, network->props.N, awork[tid], next[tid], dwork + tid * network->props.N, i, j);
			else
				graph_distance = shortestPathWeighted_v2(network->adj, network->nodes, network->props.region, network->props.N, network->obs.measures, frontier[tid], dwork + tid * network->props.N, awork[tid], i, j, network->props.use_gpu);

			//Geodesic distance
			double geodesic;
			if (network->adj[i].read(j))
				geodesic = network->obs.measures[index_map(i, j, network->props.N)];
			else
				geodesic = distance(network->nodes.crd, network->props.region, i, j, network->props.use_gpu);

			double stretch = graph_distance / geodesic;
			if (!network->props.weighted)
				stretch *= epsilon;
			samples[p] = stretch;
		}
	#ifdef _OPENMP
	}
	#endif

	//Record the mean
	network->obs.stretch = std::accumulate(samples, samples + npairs, 0.0) / npairs;

	stopwatchStop(&stp);
	printf_cyan();
	printf("Average Stretch: %.8f\n", network->obs.stretch);
	printf_std();
	printf("Elapsed Time: %.6f\n", stp.elapsedTime);
	fflush(stdout);

	//Free workspaces
	FREE(dwork, network->props.N * omp_get_max_threads(), res);
	res->hostMemUsed -= sizeof(BlockType) * awork[0].getNumBlocks() * omp_get_max_threads();
	VFREE(awork);
	FREE(samples, npairs << 1, res);
}

//High-level distance measurement subroutine
//Possibilities are:
// - Breadth first search (unweighted graphs)
// - Dijkstra's algorithm (weighted graphs)
// - A* algorithm, using geodesic distance as the heuristic (one thread per source-sink pair)
// - Hybrid A* algorithm (one thread per source)
bool measureDistances(Network *network, Resources *res, CuResources *cu, const float delta, const float epsilon, std::vector<unsigned> &Xnbr, std::vector<unsigned> &Ynbr, const unsigned X, const unsigned Y, float &dist_xy)
{
	//The user should have set the search method
	assert (network->props.sm != SEARCH_DEFAULT);

	//Allocate workspaces
	float *dwork = MALLOC<float, true>(network->props.N * omp_get_max_threads(), res);
	bool *spwork = MALLOC<bool, true>(network->props.N * omp_get_max_threads(), res);
	std::vector<unsigned> next[omp_get_max_threads()];
	uint64_t npairsXY = Xnbr.size() * Ynbr.size();

	Bitvector awork;
	for (int i = 0; i < omp_get_max_threads(); i++) {
		FastBitset fb(network->props.N);
		awork.push_back(fb);
	}
	res->hostMemUsed += sizeof(BlockType) * awork[0].getNumBlocks() * omp_get_max_threads();

	Stopwatch stp = Stopwatch();
	switch (network->props.region) {
	case S1:
	case BINARY_TREE:
	case Z2:
	case COMPLETE:
		printf("Preparing to execute Breadth First Search path finding algorithm..."); fflush(stdout);
		printf_dbg(" > Executing algorithm with [%d] threads\n", npairsXY >= 1024 ? omp_get_max_threads() : 1);
		fflush(stdout);
		stopwatchStart(&stp);
		#ifdef _OPENMP
		#pragma omp parallel for schedule (static, 16) if (npairsXY >= 1024)
		#endif
		for (uint64_t k = 0; k < npairsXY; k++) {
			unsigned tid = omp_get_thread_num();
			unsigned i = Xnbr[k / Ynbr.size()];
			unsigned j = Ynbr[k % Ynbr.size()];
			network->obs.distances[k] = (double)shortestPathUnweighted(network->adj, network->nodes.k, network->props.N, awork[tid], next[tid], dwork + tid * network->props.N, i, j);
		}
		stopwatchStop(&stp);
		printf("completed.\n");
		printf("Elapsed Time: %.6f\n", stp.elapsedTime);
		break;
	case R1:
	case T2:
	case S2:
	case BOLZA:
	{
		//Calculate dist_xy using either Dijkstra's algorithm (weighted) or the BFS algorithm (unweighted)
		if (network->props.weighted) {
			PriorityQueue<unsigned, float> frontier;
			dist_xy = shortestPathWeighted_v2(network->adj, network->nodes, network->props.region, network->props.N, network->obs.measures, frontier, dwork, awork[0], X, Y, network->props.use_gpu);
		} else
			dist_xy = epsilon * (double)shortestPathUnweighted(network->adj, network->nodes.k, network->props.N, awork[0], next[0], dwork, X, Y);

		//GA* can do either weighted or unweighted
		if (network->props.sm == GA_STAR) {
			stopwatchReset(&stp);
			printf("\nPreparing to execute GA* algorithm [%s]...\n", network->props.weighted ? "Weighted" : "Unweighted"); fflush(stdout);
			stopwatchStart(&stp);
			#ifdef CUDA_ENABLED
			shortestPathWeighted_v5(network->h_links, network->h_linkidx, network->nodes, network->obs.distances, network->obs.measures, network->obs.h_measures, network->props.N, network->props.region, network->props.weighted ? 1.0 : epsilon, network->obs.k_max, Xnbr, Ynbr, network->props.weighted, res, *cu);
			#else
			assert (false);
			#endif
			stopwatchStop(&stp);
			printf("Elapsed Time: %.6f\n", stp.elapsedTime);
		} else if (network->props.weighted) {
			if (network->props.sm == DIJKSTRA) {
				printf("Preparing to execute Dijkstra weighted path algorithm...\n");
				printf_dbg(" > Executing algorithm with [%d] threads\n", npairsXY >= 1024 ? omp_get_max_threads() : 1);
				fflush(stdout);
				stopwatchStart(&stp);
				#ifdef _OPENMP
				#pragma omp parallel for schedule (dynamic, 64) if (npairsXY >= 1024)
				#endif
				for (unsigned i = 0; i < Xnbr.size(); i++) {
					unsigned tid = omp_get_thread_num();
					shortestPathWeighted(network->adj, network->nodes, network->props.region, network->props.N, network->obs.measures, dwork + tid * network->props.N, spwork + tid * network->props.N, Xnbr[i], network->props.use_gpu);
					for (unsigned j = 0; j < Ynbr.size(); j++)
						network->obs.distances[i*Ynbr.size()+j] = (double)dwork[tid*network->props.N+Ynbr[j]];
				}
				stopwatchStop(&stp);
				printf("Elapsed Time: %.6f\n", stp.elapsedTime);
			} else if (network->props.sm == ASTAR) {
				stopwatchReset(&stp);
				printf("Preparing to execute A* weighted path algorithm...\n");
				printf_dbg(" > Executing algorithm with [%d] threads\n", npairsXY >= 1024 ? omp_get_max_threads() : 1);
				fflush(stdout);
				stopwatchStart(&stp);
				PriorityQueue<unsigned, float> frontier[omp_get_max_threads()];
				#ifdef _OPENMP
				#pragma omp parallel for schedule (dynamic, 32) if (npairsXY >= 1024)
				#endif
				for (uint64_t k = 0; k < npairsXY; k++) {
					unsigned tid = omp_get_thread_num();
					unsigned i = Xnbr[k / Ynbr.size()];
					unsigned j = Ynbr[k % Ynbr.size()];
					network->obs.distances[k] = shortestPathWeighted_v2(network->adj, network->nodes, network->props.region, network->props.N, network->obs.measures, frontier[tid], dwork + tid * network->props.N, awork[tid], i, j, network->props.use_gpu);
				}
				stopwatchStop(&stp);
				printf("Elapsed Time: %.6f\n", stp.elapsedTime);
			} else if (network->props.sm == HYBRID_ASTAR) {
				stopwatchReset(&stp);
				printf("Preparing to execute Hybrid A* algorithm...\n");
				printf_dbg(" > Executing algorithm with [%d] threads\n", Xnbr.size() >= 256 ? omp_get_max_threads() : 1);
				fflush(stdout);
				stopwatchStart(&stp);
				FastPQ<float> frontier2[omp_get_max_threads()];
				#ifdef _OPENMP
				#pragma omp parallel for schedule (dynamic, 32) if (Xnbr.size() >= 256)
				#endif
				for (unsigned i = 0; i < Xnbr.size(); i++) {
					unsigned tid = omp_get_thread_num();
					shortestPathWeighted_v3(network->adj, network->nodes, network->props.region, network->props.N, network->obs.measures, frontier2[tid], dwork + tid * network->props.N, awork[tid], Xnbr[i], Ynbr);
					for (unsigned j = 0; j < Ynbr.size(); j++)
						network->obs.distances[i*Ynbr.size()+j] = (double)dwork[tid*network->props.N+Ynbr[j]];
				}
				stopwatchStop(&stp);
				printf("Elapsed Time: %.6f\n", stp.elapsedTime);
			} else
				assert (false);
		} else {
			if (network->props.sm == BFS) {
				printf("Preparing to execute Breadth First Search path finding algorithm...\n");
				printf_dbg(" > Executing algorithm with [%d] threads\n", npairsXY >= 1024 ? omp_get_max_threads() : 1);
				fflush(stdout);
				stopwatchStart(&stp);
				#ifdef _OPENMP
				#pragma omp parallel for schedule (dynamic, 64) if (npairsXY >= 1024)
				#endif
				for (uint64_t k = 0; k < npairsXY; k++) {
					unsigned tid = omp_get_thread_num();
					unsigned i = Xnbr[k / Ynbr.size()];
					unsigned j = Ynbr[k % Ynbr.size()];
					network->obs.distances[k] = epsilon * (double)shortestPathUnweighted(network->adj, network->nodes.k, network->props.N, awork[tid], next[tid], dwork + tid * network->props.N, i, j);
				}
				stopwatchStop(&stp);
				printf("Elapsed Time: %.6f\n", stp.elapsedTime);
			} else
				assert (false);
		}

		break;
	}
	default:
		printf("Not yet implemented!\n");
		return false;
	}
	fflush(stdout);

	//Free workspaces
	FREE(dwork, network->props.N * omp_get_max_threads(), res);
	FREE(spwork, network->props.N * omp_get_max_threads(), res);

	res->hostMemUsed -= sizeof(BlockType) * awork[0].getNumBlocks() * omp_get_max_threads();
	VFREE(awork);

	return true;
}

//Measure the connected components of the graph
//This uses the Depth First Search algorithm
void measureConnectedComponents(Nodes &nodes, const Bitvector &adj, const unsigned N, Resources * const res)
{
	printf("\nMeasuring the Graph Components...\n");
	fflush(stdout);

	unsigned N_cc = 0, N_gcc = 0;
	unsigned elements;
	//Component IDs of the nodes
	nodes.cc_id = MALLOC<int, true>(N, res);

	for (unsigned i = 0; i < N; i++) {
		elements = 0;
		//Execute D.F. search if the node is not isolated
		//A component ID of 0 indicates an isolated node
		if (!nodes.cc_id[i] && !!nodes.k[i])
			dfsearch_v2(nodes, adj, N, i, ++N_cc, elements);

		//Check if this is the giant component
		if (elements > N_gcc)
			N_gcc = elements;
	}

	FREE(nodes.cc_id, N, res);

	printf_cyan();
	printf("Number of Components: %u\n", N_cc);
	printf("Size of Giant Component: %.5f\n", (double)N_gcc / N);
	printf_std();
	fflush(stdout);
}

//Return a neighborhood centered at node 'root' and containing nodes manifold-distance 'delta' or less away from 'root'
void get_ball_exact(Network *network, std::vector<unsigned> &nbr, const float delta, const unsigned root)
{
	VFREE(nbr);
	for (unsigned i = 0; i < network->props.N; i++)
		if (i != root && network->obs.measures[index_map(root, i, network->props.N)] <= delta)
			nbr.push_back(i);
}

//Return a neighborhood centered at node 'root' and containing nodes (a) weighted graph distance delta or less or (b) unweighted graph distance floor(delta/epsilon) or less away from 'root'
void get_ball(Network *network, Resources *res, std::vector<unsigned> &nbr, const float delta, const float epsilon, const unsigned root, const bool weighted)
{
	VFREE(nbr);	//Zero the vector

	std::unordered_set<unsigned> openlist, closedlist;
	FastBitset fb(network->props.N);
	uint64_t idx;
	unsigned node;

	openlist.emplace(root);

	if (weighted) {	//Use Dijkstra's algorithm
		float *dwork = MALLOC<float, true>(network->props.N, res);
		bool *spwork = MALLOC<bool, true>(network->props.N, res);
		shortestPathWeighted(network->adj, network->nodes, network->props.region, network->props.N, network->obs.measures, dwork, spwork, root, network->props.use_gpu);
		for (unsigned i = 0; i < network->props.N; i++)
			if (i != root && dwork[i] < delta)
				nbr.push_back(i);
		FREE(dwork, network->props.N, res);
		FREE(spwork, network->props.N, res);
	} else {	//Use early-exit BFS
		unsigned max_hops = (unsigned)std::floor(delta / epsilon);
		printf(" > max hops: %u\n", max_hops);

		for (unsigned i = 0; i <= max_hops; i++) {
			//Expand all nodes in the openlist
			unsigned nidx = nbr.size();
			while (!openlist.empty()) {
				openlist.erase(node = *openlist.begin());

				std::pair<std::unordered_set<unsigned>::iterator, bool> ins = closedlist.emplace(node);
				if (ins.second)	//It was inserted, so add it to the nbr list as well
					nbr.push_back(node);
			}

			//Add new neighbors to the openlist
			for (unsigned j = nidx; j < nbr.size(); j++) {
				node = nbr[j];
				network->adj[node].clone(fb);
				while (fb.any()) {
					fb.unset(idx = fb.next_bit());
					openlist.emplace((unsigned)idx);
				}

			}
		}
		nbr.erase(nbr.begin());
	}
}

#define KEYVAL(i, j) ((((uint64_t)i) << 32) | ((uint64_t)j))

//Sort sparse edge list and measures
//O(N*log(N)) Efficiency
void quicksort(std::vector<unsigned> &links0, pinned_vector<unsigned> &links1, pinned_vector<float> &measures, int64_t low, int64_t high)
{
	int64_t i, j, k;
	uint64_t key;

	if (low < high) {
		k = (low + high) >> 1;	//Pivot value
		swap(links0, links1, measures, low, k);
		key = KEYVAL(links0[low], links1[low]);
		i = low + 1;
		j = high;

		while (i <= j) {
			while ((i <= high) && (KEYVAL(links0[i], links1[i]) <= key))
				i++;
			while ((j >= low) && (KEYVAL(links0[j], links1[j]) > key))
				j--;
			if (i < j)
				swap(links0, links1, measures, i, j);
		}

		swap(links0, links1, measures, low, j);
		quicksort(links0, links1, measures, low, j - 1);
		quicksort(links0, links1, measures, j + 1, high);
	}
}

//Write data to HDF5 file format
bool write_to_h5(hid_t &handle, void *data, hsize_t dim, hsize_t *shape, hsize_t *maxshape, hsize_t *chunk_size, hid_t datatype, const char *dset_name, bool create, bool append)
{
	//NOTE: If we are appending, the only difference between runs should be the seed
	hid_t dataspace, memspace, props, dataset;
	if (create && append) {
		dataspace = H5Screate_simple(dim, shape, maxshape);
		props = H5Pcreate(H5P_DATASET_CREATE);
		H5Pset_layout(props, H5D_CHUNKED);
		H5Pset_chunk(props, dim, chunk_size);
		dataset = H5Dcreate(handle, dset_name, datatype, dataspace, H5P_DEFAULT, props, H5P_DEFAULT);
		H5Dwrite(dataset, datatype, H5S_ALL, dataspace, H5P_DEFAULT, data);
		H5Dclose(dataset);
		H5Pclose(props);
		H5Sclose(dataspace);
	} else if (create && !append) {
		dataspace = H5Screate_simple(dim, shape, NULL);
		dataset = H5Dcreate(handle, dset_name, datatype, dataspace, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		H5Dwrite(dataset, datatype, H5S_ALL, dataspace, H5P_DEFAULT, data);
		H5Dclose(dataset);
		H5Sclose(dataspace);
	} else if (!create && append) {
		dataset = H5Dopen(handle, dset_name, H5P_DEFAULT);
		dataspace = H5Dget_space(dataset);
		hsize_t dataspace_shape[dim];
		H5Sget_simple_extent_dims(dataspace, dataspace_shape, NULL);
		memspace = H5Screate_simple(dim, shape, NULL);	

		hsize_t newshape[dim];
		for (unsigned i = 0; i < dim; i++)
			newshape[i] = shape[i] + dataspace_shape[i];
		H5Dset_extent(dataset, newshape);
		dataspace = H5Dget_space(dataset);

		H5Sselect_hyperslab(dataspace, H5S_SELECT_SET, dataspace_shape, NULL, shape, NULL);
		H5Dwrite(dataset, datatype, memspace, dataspace, H5P_DEFAULT, data);
		H5Dclose(dataset);
		H5Sclose(dataspace);
		H5Sclose(memspace);
	} else
		return false;

	return true;
}

//Write simulation results to HDF5 file
bool save_to_h5(Network * const network)
{
	std::string filename = "data.h5";
	hid_t file;	//File handle
	hid_t g_node, g_degree;	//HDF5 Groups

	if (access(filename.c_str(), F_OK) == -1) {
		//The file does not exist, so we create it
		file = H5Fcreate(filename.c_str(), H5F_ACC_EXCL, H5P_DEFAULT, H5P_DEFAULT);
		g_node = H5Gcreate(file, "node", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
		g_degree = H5Gcreate(file, "degree", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

		//Create a chunked dataset, so it may later be extended
		hsize_t num_entries = 1;
		hsize_t max_entries = H5S_UNLIMITED;
		hsize_t chunk_size = 128;

		//Write average degree, Wasserstein distance, Ollivier curvature, and Ricci curvature
		write_to_h5(file, &network->obs.k_avg, 1, &num_entries, &max_entries, &chunk_size, H5T_IEEE_F64LE, "average degree", true, true);
		write_to_h5(file, &network->obs.wasserstein, 1, &num_entries, &max_entries, &chunk_size, H5T_IEEE_F64LE, "wasserstein distance", true, true);
		write_to_h5(file, &network->obs.ollivier, 1, &num_entries, &max_entries, &chunk_size, H5T_IEEE_F64LE, "ollivier curvature", true, true);
		write_to_h5(file, &network->obs.ricci, 1, &num_entries, &max_entries, &chunk_size, H5T_IEEE_F64LE, "ricci curvature", true, true);
	} else {
		//The file does exist, so we open it
		file = H5Fopen(filename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
		g_node = H5Gopen(file, "node", H5P_DEFAULT);
		g_degree = H5Gopen(file, "degree", H5P_DEFAULT);

		//Append to datasets
		hsize_t num_entries = 1;
		write_to_h5(file, &network->obs.k_avg, 1, &num_entries, NULL, NULL, H5T_IEEE_F64LE, "average degree", false, true);
		write_to_h5(file, &network->obs.wasserstein, 1, &num_entries, NULL, NULL, H5T_IEEE_F64LE, "wasserstein distance", false, true);
		write_to_h5(file, &network->obs.ollivier, 1, &num_entries, NULL, NULL, H5T_IEEE_F64LE, "ollivier curvature", false, true);
		write_to_h5(file, &network->obs.ricci, 1, &num_entries, NULL, NULL, H5T_IEEE_F64LE, "ricci curvature", false, true);
	}

	//Write nodes
	hsize_t shape[2] = { network->props.N, 2 };
	write_to_h5(g_node, network->nodes.crd->h_points, 2, shape, NULL, NULL, H5T_IEEE_F32LE, std::to_string(network->props.seed).c_str(), true, false);

	//Write degrees
	write_to_h5(g_degree, network->nodes.k, 1, shape, NULL, NULL, H5T_STD_U32LE, std::to_string(network->props.seed).c_str(), true, false);

	//Release resources
	H5Gclose(g_degree);
	H5Gclose(g_node);
	H5Fclose(file);

	return true;
}

//Write runtime parameters to log file
void write_constants(Network * const network)
{
	std::ofstream os;
	os.open("parameters.log");

	os << "RUNTIME PARAMETERS\n";
	os << "------------------\n";
	os << "ALPHA " << network->props.alpha << "\n";
	os << "BETA " << network->props.beta << "\n";
	os << "CALC_STRETCH " << (int)network->props.calc_stretch << "\n";
	os << "CHI " << network->props.chi << "\n";
	os << "CLASSIC " << (int)network->props.classic << "\n";
	os << "GPU " << (int)network->props.use_gpu << "\n";
	os << "MANIFOLD " << regions[network->props.region] << "\n";
	os << "NODES " << network->props.N << "\n";
	os << "PSI " << network->props.psi << "\n";
	os << "SEARCH_METHOD " << searchmethods[network->props.sm] << "\n";
	os << "SEED " << network->props.seed << "\n";
	os << "TARGET_SEPARATION " << separations[network->props.sep] << "\n";
	os << "WEIGHTED " << (int)network->props.weighted << "\n\n";

	os << "PREPROCESSOR CONSTANTS\n";
	os << "----------------------\n";
	#ifdef CUDA_ENABLED
	os << "GPU_BLOCK_SIZE " << GPU_BLOCK_SIZE << "\n";
	os << "GPU_MIN " << GPU_MIN << "\n";
	os << "HEAP_CAPACITY " << HEAP_CAPACITY << "\n";
	#endif
	os << "INF " << INF << "\n";
	#ifdef CUDA_ENABLED
	os << "LINKS_PER_THREAD " << LINKS_PER_THREAD << "\n";
	os << "MAX_SMALL " << MAX_SMALL << "\n";
	os << "NODE_LIST_SIZE " << NODE_LIST_SIZE << "\n";
	os << "NUM_BLOCK " << NUM_BLOCK << "\n";
	os << "NUM_THREAD " << NUM_THREAD << "\n";
	os << "NUM_TOTAL " << NUM_TOTAL << "\n";
	os << "NUM_VALUE " << NUM_VALUE << "\n";
	os << "OPEN_LIST_SIZE " << OPEN_LIST_SIZE << "\n";
	#endif
	os << "RNG_THERMAL " << RNG_THERMAL << "\n";
	os << "TOL " << TOL << "\n";
	#ifdef CUDA_ENABLED
	os << "VALUE_PER_THREAD " << VALUE_PER_THREAD << "\n\n";
	#endif

	os << "COMPILATION FLAGS\n";
	os << "-----------------\n";
	os << "AVX2_ENABLED ";
	#ifdef AVX2_ENABLED
	os << "1\n";
	#else
	os << "0\n";
	#endif
	os << "CUDA_ENABLED ";
	#ifdef CUDA_ENABLED
	os << "1\n";
	os << "GLOB_MEM " << GLOB_MEM << "\n";
	#else
	os << "0\n";
	#endif
	os << "HAVE_CXX11 " << (int)HAVE_CXX11 << "\n";
	os << "HAVE_HDF5 " << (int)HAVE_HDF5 << "\n";
	os << "MAX_GPUS " << MAX_GPUS << "\n";
	os << "_OPENMP ";
	#ifdef _OPENMP
	os << "1\n";
	#else
	os << "0\n";
	#endif
	os << "VERSION " << VERSION << "\n";

	os.flush();
	os.close();
}
