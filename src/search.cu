#include "search.h"
#ifdef CUDA_ENABLED
#include "kernels/search_kernels.cuh"
#endif

/////////////////////////////
//(C) Will Cunningham 2019 //
//    Perimeter Institute  //
/////////////////////////////

//Depth First Search
//O(N+E) Efficiency
void dfsearch_v2(const Nodes &nodes, const Bitvector &adj, const unsigned N, const unsigned index, const int id, unsigned &elements)
{
	//The array 'cc_id' contains component labels
	//An id of zero indicates it has not been added to a component
	nodes.cc_id[index] = id;
	//'elements' counts the number of nodes in component labeled by 'id'
	elements++;

	for (unsigned i = 0; i < N; i++)
		if (adj[index].read(i) && !nodes.cc_id[i])
			dfsearch_v2(nodes, adj, N, i, id, elements);
}

//Breadth first search algorithm
int shortestPathUnweighted(Bitvector &adj, const unsigned * const k, const unsigned N, FastBitset &awork, std::vector<unsigned> &next, float * const dwork, const unsigned source, const unsigned sink)
{
	int shortest = -1;
	next.clear();
	next.push_back(source);
	std::fill(dwork, dwork + N, -1);
	dwork[source] = 0;

	uint64_t idx = 0;
	while (!!next.size()) {
		int loc_min = INT_MAX, loc_min_idx = -1;
		for (size_t i = 0; i < next.size(); i++) {
			if (dwork[next[i]] >= 0 && dwork[next[i]] < loc_min) {
				loc_min = dwork[next[i]];
				loc_min_idx = i;
			}
		}

		int v = next[loc_min_idx];
		next.erase(next.begin() + loc_min_idx);
		adj[v].clone(awork);
		for (unsigned i = 0; i < k[v]; i++) {
			awork.unset(idx = awork.next_bit());
			if (dwork[idx] == -1) {
				dwork[idx] = dwork[v] + 1;
				if (idx != sink)
					next.push_back(idx);
				else
					goto PathExit;
			}
		}
	}

	PathExit:
	shortest = (int)dwork[sink];

	return shortest;
}

//Dijkstra's algorithm, O(N^2) for single-pair
void shortestPathWeighted(Bitvector &adj, const Nodes &nodes, const Region region, const unsigned N, const float * const measures, float * const dwork, bool * const spwork, const unsigned source, const bool gpu)
{
	std::fill(dwork, dwork + N, INF);
	memset(spwork, 0, sizeof(bool) * N);

	dwork[source] = 0;

	for (unsigned i = 0; i < N - 1; i++) {
		unsigned u = minDistance(dwork, spwork, N);
		spwork[u] = true;

		for (unsigned j = 0; j < N; j++) {
			float d = (u == j) ? 0.0 : measures[index_map(u, j, N)];
			if (!spwork[j] && adj[u].read(j) && !IS_INF(dwork[u]) && dwork[u] + d < dwork[j])
				dwork[j] = dwork[u] + d;
		}
	}
}

//A* algorithm with priority queue, O((E+N)*log(N))
float shortestPathWeighted_v2(Bitvector &adj, const Nodes &nodes, const Region region, const unsigned N, const float * const measures, PriorityQueue<unsigned, float> &frontier, float * const dwork, FastBitset &neighbors, const unsigned source, const unsigned sink, const bool gpu)
{
	if (source == sink)
		return 0.0;

	std::fill(dwork, dwork + N, INF);
	frontier.clear();
	frontier.put(source, 0.0);
	dwork[source] = 0.0;

	while (!frontier.empty()) {
		unsigned current = frontier.get();
		if (current == sink)
			break;

		adj[current].clone(neighbors);
		uint64_t next;
		while (neighbors.any()) {
			neighbors.unset(next = neighbors.next_bit());
			float d = dwork[current] + measures[index_map(current, next, N)];
			if (IS_INF(dwork[next]) || d < dwork[next]) {
				dwork[next] = d;
				float priority = d;
				if (next != sink)
					d += measures[index_map(next, sink, N)];
				frontier.put(next, priority);
			}
		}
	}

	return dwork[sink];
}

//Modified A* algorithm
//Use a list of sinks, reached in any order
void shortestPathWeighted_v3(Bitvector &adj, const Nodes &nodes, const Region region, const unsigned N, const float * const measures, FastPQ<float> &frontier, float * const dwork, FastBitset &neighbors, const unsigned source, std::vector<unsigned> sinks)
{
	std::fill(dwork, dwork + N, INF);
	frontier.clear();
	frontier.emplace(std::make_pair(0.0, source));
	frontier.refresh();
	dwork[source] = 0.0;

	while (!frontier.empty() && !sinks.empty()) {
		unsigned current = frontier.top().second;
		frontier.pop();
		std::vector<unsigned>::iterator index;
		if ((index = std::find(sinks.begin(), sinks.end(), current)) != sinks.end()) {
			//We have reached one of the sinks
			if (!std::distance(index, sinks.begin())) {
				//Remove it from the list
				sinks.erase(index);
				if (!sinks.size())
					break;

				//If it was the intended sink, update priorities
				for (size_t i = 0; i < frontier.size(); i++) {
					if (frontier.get(i).second != sinks.front())
						frontier.update(i, dwork[frontier.get(i).second] + measures[index_map(frontier.get(i).second, sinks.front(), N)]);
				}
				frontier.refresh();
			} 
		}

		adj[current].clone(neighbors);
		uint64_t next;
		while (neighbors.any()) {
			neighbors.unset(next = neighbors.next_bit());
			float d = dwork[current] + measures[index_map(current, next, N)];
			if (IS_INF(dwork[next]) || d < dwork[next]) {
				dwork[next] = d;
				if (next == sinks.front())
					frontier.emplace(std::make_pair(d, next));
				else
					frontier.emplace(std::make_pair(d + measures[index_map(next, sinks.front(), N)], next));
			}
		}
		frontier.refresh();
	}
}

//One iteration in the GA* search algorithm (defined below)
#ifdef CUDA_ENABLED
template<Region region, bool weighted>
void GA_step(GA_DATA &d, std::vector<std::priority_queue<heap_t, std::vector<heap_t>, std::greater<heap_t>>> &pq, double * const distances, node_t * const h_nodes, std::vector<unsigned> &h_hash, std::vector<heap_t> &h_optimalnodes, const unsigned N, const unsigned source, std::vector<std::pair<unsigned,unsigned>> &psinks, unsigned &sinkidx, curvature_context_t &context, float &shortest, const float epsilon, const unsigned k_max, unsigned &round, bool &solved, Stopwatch * const watches)
{
	unsigned sink = psinks[sinkidx].first;
	int os;
	unsigned h_mindist;
	unsigned solution;
	int ns = -1;
	int ss, ss2;
	int hs;
	bool use_old;

	stopwatchStart(&watches[1]);
	//Extract the nodes which are currently in the openlist
	extract_v2<NUM_BLOCK, NUM_THREAD, HEAP_CAPACITY, weighted><<<NUM_BLOCK, NUM_THREAD>>>((node_t*)d.nodes, (unsigned*)d.links, (uint64_t*)d.linkidx, (float*)d.measures, (heap_t*)d.openlist, (int*)d.heapsize, (unsigned*)d.mindist, (sort_t*)d.sortlist, (int*)d.sortlistsize, (int*)d.heapindex, (int*)d.heapinsertsize, (heap_t*)d.optimalnodes, (int*)d.optimalsize, N, epsilon, sink);
	checkCudaErrors(cuCtxSynchronize());
	stopwatchStop(&watches[1]);

	//Return to this point when a new solution is found
	//A 'solution' is found when one of the targets in 'psinks' has been reached
	//and the heuristics have been updated
	NewSolution:
	checkCudaErrors(cuMemcpyDtoH(&os, d.optimalsize, sizeof(int)));
	//The variable 'os' counts how many of the sinks have been visited in a single iteration
	assert (os < NUM_TOTAL);

	checkCudaErrors(cuMemcpyDtoH(&h_mindist, d.mindist, sizeof(int)));

	use_old = false;
	//If true, the search went one step too far
	if (ns != -1 && pq[psinks[sinkidx].second].size() && u2f(h_mindist) > pq[psinks[sinkidx].second].top().f) {
		use_old = true;
		solution = f2u(pq[psinks[sinkidx].second].top().f);
	}

	//If there are more sinks to visit
	if (!!os || use_old) {
		if (!use_old) {
			stopwatchStart(&watches[7]);
			//The array 'optimalnodes' stores the shortest paths to each node
			checkCudaErrors(cuMemcpyDtoH(h_optimalnodes.data(), d.optimalnodes, sizeof(heap_t) * os));
			checkCudaErrors(cuMemcpyDtoH(&solution, d.mindist, sizeof(unsigned)));
			checkCudaErrors(cuMemcpyDtoH(&ns, d.nodesize, sizeof(int)));
			assert (ns < NODE_LIST_SIZE);
			//Record the shorest paths to each sink that was reached in this iteration
			for (int i = 0; i < os; i++)
				pq[psinks[sinkidx].second].push(h_optimalnodes[i]);
			stopwatchStop(&watches[7]);
		}

		if (f2u(pq[psinks[sinkidx].second].top().f) <= solution) {
			shortest = pq[psinks[sinkidx].second].top().f;
			distances[psinks[sinkidx].second] = shortest;	//Record the distance

			//Update Heuristics:
			if (++sinkidx < psinks.size()) {
				sink = psinks[sinkidx].first;
				//Check if the new sink was already visited
				checkCudaErrors(cuMemsetD32(d.mindist, UINT32_MAX, 1));
				checkCudaErrors(cuMemsetD32(d.optimalsize, 0, 1));
				checkCudaErrors(cuCtxSynchronize());
				stopwatchStart(&watches[5]);
				updateHeuristics<region, 16*NUM_BLOCK, NUM_THREAD, HEAP_CAPACITY,weighted><<<16*NUM_BLOCK, NUM_THREAD>>>((node_t*)d.nodes, (int*)d.nodesize, (unsigned*)d.hash, (heap_t*)d.openlist, (int*)d.heapsize, (int*)d.heapindex, (heap_t*)d.heapinsertlist, (int*)d.heapinsertsize, (unsigned*)d.mindist, (heap_t*)d.optimalnodes, (int*)d.optimalsize, os, (cuComplex*)d.z, (float4*)d.generators, epsilon, sink);
				checkCudaErrors(cuCtxSynchronize());
				stopwatchStop(&watches[5]);

				stopwatchStart(&watches[7]);
				//Update the priority queue
				checkCudaErrors(cuMemcpyDtoH(h_nodes, d.nodes, sizeof(node_t) * ns));
				for (unsigned i = sinkidx; i < psinks.size(); i++) {
					std::priority_queue<heap_t, std::vector<heap_t>, std::greater<heap_t>> tmp;
					while (!pq[psinks[i].second].empty()) {
						heap_t heap = pq[psinks[i].second].top();
						pq[psinks[i].second].pop();
						heap.f = h_nodes[heap.addr].f;
						tmp.emplace(heap);
					}
					pq[psinks[i].second] = tmp;
				}
				stopwatchStop(&watches[7]);

				//Check to see if the solution is optimal by returning to 'NewSolution'
				checkCudaErrors(cuMemcpyDtoH(&solution, d.mindist, sizeof(unsigned)));

				goto NewSolution;
			} else {
				//All sinks have been visited now
				solved = true;
				return;
			}
		}
	}

	//Sort the extracted nodes using Thrust's 'sort' function
	checkCudaErrors(cuMemcpyDtoH(&ss, d.sortlistsize, sizeof(int)));
	assert (ss < NUM_VALUE * k_max);

	stopwatchStart(&watches[6]);
	thrust::device_ptr<sort_t> t_sortlist((sort_t*)d.sortlist);
	thrust::sort(t_sortlist, t_sortlist + ss);
	checkCudaErrors(cuCtxSynchronize());
	stopwatchStop(&watches[6]);

	//Assign extracted (now sorted) nodes to new priority queues
	stopwatchStart(&watches[2]);
	assign<NUM_THREAD><<<div_up(ss, NUM_THREAD), NUM_THREAD>>>((sort_t*)d.sortlist, ss, (sort_t*)d.sortlist2, (int*)d.sortlistsize2);
	checkCudaErrors(cuCtxSynchronize());
	stopwatchStop(&watches[2]);

	checkCudaErrors(cuMemcpyDtoH(&ss2, d.sortlistsize2, sizeof(int)));
	assert (ss2 < NUM_VALUE * k_max);

	//Remove duplicates (parallel hashing)
	stopwatchStart(&watches[3]);
	deduplicate<region,NUM_THREAD,weighted><<<div_up(ss2, NUM_THREAD), NUM_THREAD>>>((node_t*)d.nodes, (int*)d.nodesize, (unsigned*)d.hash, (sort_t*)d.sortlist2, ss2, (heap_t*)d.heapinsertlist, (int*)d.heapinsertsize, (cuComplex*)d.z, (float4*)d.generators, (unsigned*)d.k, epsilon, sink);
	checkCudaErrors(cuCtxSynchronize());
	stopwatchStop(&watches[3]);

	stopwatchStart(&watches[7]);
	checkCudaErrors(cuMemcpyDtoH(h_hash.data(), d.hash, sizeof(unsigned) * N));
	checkCudaErrors(cuMemcpyDtoH(&ns, d.nodesize, sizeof(int)));
	assert (ns < NODE_LIST_SIZE);
	checkCudaErrors(cuMemcpyDtoH(h_nodes, d.nodes, sizeof(node_t) * ns));
	for (unsigned i = sinkidx + 1; i < psinks.size(); i++) {
		if (h_hash[psinks[i].first] != UINT32_MAX) {
			float sol = h_nodes[h_hash[psinks[i].first]].g;
			heap_t h;
			h.addr = h_hash[psinks[i].first];
			h.f = sol;
			pq[psinks[i].second].push(h);
		}
	}
	stopwatchStop(&watches[7]);

	checkCudaErrors(cuMemcpyDtoH(&hs, d.heapinsertsize, sizeof(int)));
	assert (hs < NUM_VALUE * k_max);

	//Heap Insert
	//Nodes are now inserted back into the openlist according to the specified assignments
	stopwatchStart(&watches[4]);
	heapinsert<NUM_BLOCK, NUM_THREAD, HEAP_CAPACITY><<<NUM_BLOCK, NUM_THREAD>>>((heap_t*)d.openlist, (int*)d.heapsize, (int*)d.heapindex, (heap_t*)d.heapinsertlist, (int*)d.heapinsertsize, (int*)d.sortlistsize, (int*)d.sortlistsize2, (unsigned*)d.mindist, (int*)d.optimalsize);
	checkCudaErrors(cuCtxSynchronize());
	stopwatchStop(&watches[4]);
}

//GA* Algorithm
//The GPUs will evenly divide the sources among themselves
bool shortestPathWeighted_v5(pinned_vector<unsigned> &h_links, pinned_vector<uint64_t> h_linkidx, const Nodes &nodes, double * const distances, float * const measures, pinned_vector<float> &h_measures, const unsigned N, const Region region, const float epsilon, const unsigned k_max, std::vector<unsigned> &sources, std::vector<unsigned> &sinks, const bool weighted, Resources * res, CuResources &cu)
{
	assert (VALUE_PER_THREAD == 1);
	assert (region == T2 || region == S2 || region == BOLZA);

	cu.gpus_active = std::min(sources.size(), (size_t)MAX_GPUS);

	if (MAX_GPUS == 1)
		cuCtxPopCurrent(cu.cuContext);
	curvature_context_t context(cu);

	if (context.gpus_active() == 1)
		context.push_device(0);

	uint64_t nlinks = 0;
	for (int i = 0; i < N; i++)
		nlinks += nodes.k[i];

	//Data structures needed for GPU
	GA_DATA d[MAX_GPUS];
	node_t *h_nodes[MAX_GPUS];

	//Allocate data structures for GPU subroutines
	for (unsigned i = 0; i < context.gpus_active(); i++) {
		if (context.gpus_active() > 1)
			context.push_device(i);

		d[i].z = CUDA_MALLOC<cuComplex>(N, res, (cuComplex*)nodes.crd->h_points);
		d[i].k = CUDA_MALLOC<unsigned>(N, res, nodes.k);
		d[i].links = CUDA_MALLOC<unsigned>(nlinks, res, h_links.data());
		d[i].linkidx = CUDA_MALLOC<uint64_t>(N, res, h_linkidx.data());
		d[i].measures = CUDA_MALLOC<float>(nlinks, res, h_measures.data());
		if (region == BOLZA)
			d[i].generators = CUDA_MALLOC<float4>(48, res, (float4*)generators);
		d[i].nodes = CUDA_MALLOC<node_t>(NODE_LIST_SIZE, res);
		d[i].nodesize = CUDA_MALLOC<int, 1>(1, res);
		d[i].hash = CUDA_MALLOC<unsigned, UINT32_MAX>(N, res);
		d[i].openlist = CUDA_MALLOC<heap_t>(OPEN_LIST_SIZE, res);
		d[i].heapsize = CUDA_MALLOC<int>(NUM_TOTAL, res);
		d[i].heapindex = CUDA_MALLOC<int>(1, res);
		d[i].sortlist = CUDA_MALLOC<sort_t>(NUM_VALUE * k_max, res);
		d[i].sortlist2 = CUDA_MALLOC<sort_t>(NUM_VALUE * k_max, res);
		d[i].sortlistsize = CUDA_MALLOC<int>(1, res);
		d[i].sortlistsize2 = CUDA_MALLOC<int>(1, res);
		d[i].heapinsertlist = CUDA_MALLOC<heap_t>(NUM_VALUE * k_max, res);
		d[i].heapinsertsize = CUDA_MALLOC<int>(1, res);
		d[i].mindist = CUDA_MALLOC<unsigned, UINT32_MAX>(1, res);
		d[i].optimalnodes = CUDA_MALLOC<heap_t>(NUM_TOTAL, res);
		d[i].optimalsize = CUDA_MALLOC<int>(1, res);
		h_nodes[i] = MALLOC<node_t, true>(NODE_LIST_SIZE, res);

		if (context.gpus_active() > 1)
			context.pop_device(i);
	}

	memoryCheckpoint(res);
	printMemUsed("for GA* Search", res->hostMemUsed, res->devMemUsed, 0);

	unsigned sources_per_gpu = ceil((float)sources.size() / context.gpus_active());
	context.sync_all_gpus();

	Stopwatch stp = Stopwatch();
	stopwatchStart(&stp);

	Stopwatch watches[8];
	for (int i = 0; i < 7; i++)
		watches[i] = Stopwatch();

	printf_dbg(" > Executing algorithm with [%lu] threads across [%u] GPU(s)\n", (long)NUM_BLOCK * NUM_THREAD * context.gpus_active(), context.gpus_active());
	fflush(stdout);

	#pragma omp parallel firstprivate(context) num_threads(context.gpus_active()) if (context.gpus_active() > 1)
	{
		unsigned tid = omp_get_thread_num();
		unsigned nsources = (tid == omp_get_num_threads() - 1) ? sources.size() - tid * sources_per_gpu : sources_per_gpu;

		if (context.gpus_active() > 1)
			context.push_device(tid);

		std::vector<heap_t> h_optimalnodes(NUM_TOTAL);
		std::vector<unsigned> h_hash(N);
		std::vector<std::priority_queue<heap_t, std::vector<heap_t>, std::greater<heap_t>>> pq(sinks.size());
		std::vector<std::pair<unsigned, unsigned>> psinks;
		psinks.reserve(sinks.size());

		//For each source assigned to this GPU...
		for (unsigned i = 0; i < nsources; i++) {
			//Initialize priority queue, sinks, and distance matrix
			stopwatchStart(&watches[7]);
			pq.clear();
			pq.resize(sinks.size());
			psinks.clear();
			for (unsigned j = 0; j < sinks.size(); j++) {
				if (sinks[j] != sources[tid*sources_per_gpu+i])
					psinks.push_back(std::make_pair(sinks[j], j));
				else
					distances[(tid*sources_per_gpu+i)*sinks.size()+j] = 0.0;
			}
			stopwatchStop(&watches[7]);

			unsigned round = 0;
			bool solved = false;
			float shortest = INF;
			unsigned sinkidx = 0;

			//Initialize the distances on the GPU
			//The initial values are simply the heuristics (continuum geodesic distance)
			//We use templates to avoid branching in the CUDA kernels
			stopwatchStart(&watches[0]);
			if (weighted) {
				if (region == T2)
					initGAstar<T2, true><<<1,1>>>((node_t*)d[tid].nodes, (unsigned*)d[tid].hash, (heap_t*)d[tid].openlist, (int*)d[tid].heapsize, (cuComplex*)d[tid].z, (float4*)d[tid].generators, (unsigned*)d[tid].k, sources[tid*sources_per_gpu+i], psinks[0].first, epsilon);
				else if (region == S2)
					initGAstar<S2, true><<<1,1>>>((node_t*)d[tid].nodes, (unsigned*)d[tid].hash, (heap_t*)d[tid].openlist, (int*)d[tid].heapsize, (cuComplex*)d[tid].z, (float4*)d[tid].generators, (unsigned*)d[tid].k, sources[tid*sources_per_gpu+i], psinks[0].first, epsilon);
				else if (region == BOLZA)
					initGAstar<BOLZA, true><<<1,1>>>((node_t*)d[tid].nodes, (unsigned*)d[tid].hash, (heap_t*)d[tid].openlist, (int*)d[tid].heapsize, (cuComplex*)d[tid].z, (float4*)d[tid].generators, (unsigned*)d[tid].k, sources[tid*sources_per_gpu+i], psinks[0].first, epsilon);
			} else {
				if (region == T2)
					initGAstar<T2, false><<<1,1>>>((node_t*)d[tid].nodes, (unsigned*)d[tid].hash, (heap_t*)d[tid].openlist, (int*)d[tid].heapsize, (cuComplex*)d[tid].z, (float4*)d[tid].generators, (unsigned*)d[tid].k, sources[tid*sources_per_gpu+i], psinks[0].first, epsilon);
				else if (region == S2)
					initGAstar<S2, false><<<1,1>>>((node_t*)d[tid].nodes, (unsigned*)d[tid].hash, (heap_t*)d[tid].openlist, (int*)d[tid].heapsize, (cuComplex*)d[tid].z, (float4*)d[tid].generators, (unsigned*)d[tid].k, sources[tid*sources_per_gpu+i], psinks[0].first, epsilon);
				else if (region == BOLZA)
					initGAstar<BOLZA, false><<<1,1>>>((node_t*)d[tid].nodes, (unsigned*)d[tid].hash, (heap_t*)d[tid].openlist, (int*)d[tid].heapsize, (cuComplex*)d[tid].z, (float4*)d[tid].generators, (unsigned*)d[tid].k, sources[tid*sources_per_gpu+i], psinks[0].first, epsilon);
			}
			checkCudaErrors(cuCtxSynchronize());
			stopwatchStop(&watches[0]);

			//Continually move one step outward from the source node
			//until all targets have been reached
			while (!solved) {
				if (weighted) {
					if (region == T2)
						GA_step<T2, true>(d[tid], pq, distances + (tid * sources_per_gpu + i) * sinks.size(), h_nodes[tid], h_hash, h_optimalnodes, N, sources[tid*sources_per_gpu+i], psinks, sinkidx, context, shortest, epsilon, k_max, round, solved, watches);
					else if (region == S2)
						GA_step<S2, true>(d[tid], pq, distances + (tid * sources_per_gpu + i) * sinks.size(), h_nodes[tid], h_hash, h_optimalnodes, N, sources[tid*sources_per_gpu+i], psinks, sinkidx, context, shortest, epsilon, k_max, round, solved, watches);
					else if (region == BOLZA)
						GA_step<BOLZA, true>(d[tid], pq, distances + (tid * sources_per_gpu + i) * sinks.size(), h_nodes[tid], h_hash, h_optimalnodes, N, sources[tid*sources_per_gpu+i], psinks, sinkidx, context, shortest, epsilon, k_max, round, solved, watches);
				} else {
					if (region == T2)
						GA_step<T2, false>(d[tid], pq, distances + (tid * sources_per_gpu + i) * sinks.size(), h_nodes[tid], h_hash, h_optimalnodes, N, sources[tid*sources_per_gpu+i], psinks, sinkidx, context, shortest, epsilon, k_max, round, solved, watches);
					else if (region == S2)
						GA_step<S2, false>(d[tid], pq, distances + (tid * sources_per_gpu + i) * sinks.size(), h_nodes[tid], h_hash, h_optimalnodes, N, sources[tid*sources_per_gpu+i], psinks, sinkidx, context, shortest, epsilon, k_max, round, solved, watches);
					else if (region == BOLZA)
						GA_step<BOLZA, false>(d[tid], pq, distances + (tid * sources_per_gpu + i) * sinks.size(), h_nodes[tid], h_hash, h_optimalnodes, N, sources[tid*sources_per_gpu+i], psinks, sinkidx, context, shortest, epsilon, k_max, round, solved, watches);
				}
				round++;
			}

			//Reset data structures for a new source
			if (i < nsources - 1) {
				stopwatchStart(&watches[7]);
				memset(h_nodes[tid], 0, sizeof(node_t) * NODE_LIST_SIZE);
				checkCudaErrors(cuMemsetD32Async(d[tid].nodes, 0, NODE_LIST_SIZE << 2, 0));
				checkCudaErrors(cuMemsetD32Async(d[tid].nodesize, 1, 1, 0));
				checkCudaErrors(cuMemsetD32Async(d[tid].openlist, 0, OPEN_LIST_SIZE << 1, 0));
				checkCudaErrors(cuMemsetD32Async(d[tid].hash, UINT32_MAX, N, 0));
				checkCudaErrors(cuMemsetD32Async(d[tid].heapsize, 0, NUM_TOTAL, 0));
				checkCudaErrors(cuMemsetD32Async(d[tid].heapindex, 0, 1, 0));
				checkCudaErrors(cuMemsetD32Async(d[tid].sortlist, 0, NUM_VALUE * k_max << 1, 0));
				checkCudaErrors(cuMemsetD32Async(d[tid].sortlist2, 0, NUM_VALUE * k_max << 1, 0));
				checkCudaErrors(cuMemsetD32Async(d[tid].sortlistsize, 0, 1, 0));
				checkCudaErrors(cuMemsetD32Async(d[tid].sortlistsize2, 0, 1, 0));
				checkCudaErrors(cuMemsetD32Async(d[tid].heapinsertlist, 0, NUM_VALUE * k_max << 1, 0));
				checkCudaErrors(cuMemsetD32Async(d[tid].heapinsertsize, 0, 1, 0));
				checkCudaErrors(cuMemsetD32Async(d[tid].mindist, UINT32_MAX, 1, 0));
				checkCudaErrors(cuMemsetD32Async(d[tid].optimalnodes, 0, NUM_TOTAL << 1, 0));
				checkCudaErrors(cuMemsetD32Async(d[tid].optimalsize, 0, 1, 0));
				checkCudaErrors(cuCtxSynchronize());
				stopwatchStop(&watches[7]);
			}
		}

		if (context.gpus_active() > 1)
			context.pop_device(tid);
	}
	context.sync_all_gpus();

	stopwatchStop(&stp);
	if (context.gpus_active() == 1) {
		printf_dbg("Elapsed:        %.6f\n", stp.elapsedTime);
		printf("Initialization: %.6f\n", watches[0].elapsedTime);
		printf("Extraction:     %.6f\n", watches[1].elapsedTime);
		printf("Merge:          %.6f\n", watches[6].elapsedTime);
		printf("Assign:         %.6f\n", watches[2].elapsedTime);
		printf("Deduplication:  %.6f\n", watches[3].elapsedTime);
		printf("Insertion:      %.6f\n", watches[4].elapsedTime);
		printf("Update:         %.6f\n", watches[5].elapsedTime);
		printf("Misc:           %.6f\n", watches[7].elapsedTime);
	}

	//Free GPU memory
	for (unsigned i = 0; i < context.gpus_active(); i++) {
		if (context.gpus_active() > 1)
			context.push_device(i);

		CUDA_FREE<cuComplex>(d[i].z, N, res);
		CUDA_FREE(d[i].k, N, res);
		CUDA_FREE(d[i].links, nlinks, res);
		CUDA_FREE<uint64_t>(d[i].linkidx, N, res);
		CUDA_FREE(d[i].measures, nlinks, res);
		if (region == BOLZA)
			CUDA_FREE<float4>(d[i].generators, 48, res);

		CUDA_FREE<node_t>(d[i].nodes, NODE_LIST_SIZE, res);
		CUDA_FREE(d[i].nodesize, 1, res);
		CUDA_FREE<heap_t>(d[i].openlist, OPEN_LIST_SIZE, res);
		CUDA_FREE(d[i].hash, N, res);
		CUDA_FREE(d[i].heapsize, NUM_TOTAL, res);
		CUDA_FREE(d[i].heapindex, 1, res);

		CUDA_FREE<sort_t>(d[i].sortlist, NUM_VALUE * k_max, res);
		CUDA_FREE<sort_t>(d[i].sortlist2, NUM_VALUE * k_max, res);
		CUDA_FREE(d[i].sortlistsize, 1, res);
		CUDA_FREE(d[i].sortlistsize2, 1, res);

		CUDA_FREE<heap_t>(d[i].heapinsertlist, NUM_VALUE * k_max, res);
		CUDA_FREE(d[i].heapinsertsize, 1, res);

		CUDA_FREE(d[i].mindist, 1, res);
		CUDA_FREE<heap_t>(d[i].optimalnodes, NUM_TOTAL, res);
		CUDA_FREE(d[i].optimalsize, 1, res);
		FREE(h_nodes[i], NODE_LIST_SIZE, res);

		if (context.gpus_active() > 1)
			context.pop_device(i);
	}

	if (context.gpus_active() == 1) {
		context.pop_device(0);
		if (MAX_GPUS == 1)
			cuCtxPushCurrent(cu.cuContext[0]);
	}

	printf("\n");

	return true;
}
#endif
